<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */

/**
 * directorymanager_DirectoryEntryExtraSet
 *
 * @property ORM_IntField       $id_fieldx
 * @property directorymanager_DirectoryEntry       $id_entry
 * @property ORM_StringField    $field_value
 */
class directorymanager_DirectoryEntryExtraSet extends ORM_RecordSet
{
    public function __construct()
    {
        require_once $GLOBALS['babInstallPath'].'utilit/dirincl.php';
        
        parent::__construct();
        
        $App = directorymanager_App();
        
        $this->setTableName(BAB_DBDIR_ENTRIES_EXTRA_TBL);
        $this->setPrimaryKey('id');
        
        $this->addFields(
            ORM_IntField('id_fieldx'),
            ORM_StringField('field_value')
        );
        
        $this->addFields(
            $this->concat(
                'babdirf',
                $this->id_fieldx
            )->setName('babdirid')
        );
        
        $this->hasOne('id_entry', $App->DirectoryEntrySetClassName());
    }
}

/**
 * directorymanager_DirectoryEntryExtra
 *
 * @property int        $id_fieldx
 * @property directorymanager_DirectoryEntry        $id_entry
 * @property string     $field_value
 * @property string     $babdirid
 */
class directorymanager_DirectoryEntryExtra extends ORM_Record
{    
   public function getName()
   {
       global $babDB;
       
       $req = "select * from ".BAB_DBDIR_FIELDSEXTRA_TBL." where id=".$babDB->quote($this->id_fieldx);
       $fieldsExtra = $babDB->db_query($req);
       
       while($row = $babDB->db_fetch_assoc($fieldsExtra))
       {
           $req = "select * from ".BAB_DBDIR_FIELDS_DIRECTORY_TBL." where id=".$babDB->quote($row['id_field'] - BAB_DBDIR_MAX_COMMON_FIELDS);
           $fieldInfos = $babDB->db_fetch_array($babDB->db_query($req));
           if($fieldInfos && isset($fieldInfos['name'])){
               return $fieldInfos['name'];
           }
       }
       
       return '';
   }
}