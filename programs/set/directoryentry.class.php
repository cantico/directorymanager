<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */

/**
 * directorymanager_DirectoryEntrySet
 *
 * @property ORM_StringField    $cn
 * @property ORM_StringField    $sn
 * @property ORM_StringField    $mn
 * @property ORM_StringField    $givenname
 * @property ORM_StringField    $jpegphoto
 * @property ORM_EmailField     $email
 * @property ORM_StringField    $btel
 * @property ORM_StringField    $mobile
 * @property ORM_StringField    $htel
 * @property ORM_StringField    $bfax
 * @property ORM_StringField    $title
 * @property ORM_StringField    $departmentnumber
 * @property ORM_StringField    $organisationname
 * @property ORM_TextField      $bstreetaddress
 * @property ORM_StringField    $bcity
 * @property ORM_StringField    $bpostalcode
 * @property ORM_StringField    $bstate
 * @property ORM_StringField    $bcountry
 * @property ORM_TextField      $hstreetaddress
 * @property ORM_StringField    $hcity
 * @property ORM_StringField    $hpostalcode
 * @property ORM_StringField    $hstate
 * @property ORM_StringField    $hcountry
 * @property ORM_TextField      $user1
 * @property ORM_TextField      $user2
 * @property ORM_TextField      $user3
 * @property ORM_TextField      $photo_data
 * @property ORM_StringField    $photo_type
 * @property ORM_IntField       $id_directory
 * @property ORM_UserField      $id_user
 * @property ORM_DatetimeField  $date_modification
 * @property ORM_UserField      $id_modifiedby
 */
class directorymanager_DirectoryEntrySet extends ORM_RecordSet
{
    public function __construct()
    {
        require_once $GLOBALS['babInstallPath'].'utilit/dirincl.php';
        
        parent::__construct();
        
        $this->setDescription('Entry');
        
        $this->setTableName('bab_dbdir_entries');
        $this->setPrimaryKey('id');
        
        $this->addFields(
            ORM_StringField('cn'),
            ORM_StringField('sn'),
            ORM_StringField('mn'),
            ORM_StringField('givenname'),
            ORM_StringField('jpegphoto'),
            ORM_EmailField('email'),
            ORM_StringField('btel'),
            ORM_StringField('mobile'),
            ORM_StringField('htel'),
            ORM_StringField('bfax'),
            ORM_StringField('title'),
            ORM_StringField('departmentnumber'),
            ORM_StringField('organisationname'),
            ORM_TextField('bstreetaddress'),
            ORM_StringField('bcity'),
            ORM_StringField('bpostalcode', 10),
            ORM_StringField('bstate'),
            ORM_StringField('bcountry'),
            ORM_TextField('hstreetaddress'),
            ORM_StringField('hcity'),
            ORM_StringField('hpostalcode', 10),
            ORM_StringField('hstate'),
            ORM_StringField('hcountry'),
            ORM_TextField('user1'),
            ORM_TextField('user2'),
            ORM_TextField('user3'),
            ORM_TextField('photo_data'),
            ORM_StringField('photo_type', 20),
            ORM_IntField('id_directory'),
            ORM_UserField('id_user'),
            ORM_DatetimeField('date_modification'),
            ORM_UserField('id_modifiedby')
        );
        
        $App = directorymanager_App();
        $App->includeDirectoryEntryExtraSet();
        $this->hasMany('extrafields', $App->DirectoryEntryExtraSetClassName(), 'id_entry');
    }
    
    /**
     * The entry is readable if the user is in the same directory or the same group
     * @return ORM_Criteria
     */
    public function isReadable(){
        $views = bab_getUserIdObjects(BAB_DBDIRVIEW_GROUPS_TBL);
        return $this->id_directory->in($views)->_OR_($this->id_user->in($this->getGroupUserIds()));
    }
    
    public function isUpdatable(){
        $views = bab_getUserIdObjects(BAB_DBDIRUPDATE_GROUPS_TBL);
        return $this->id_directory->in($views);
    }
    
    public function isCreatable(){
        $views = bab_getUserIdObjects(BAB_DBDIRADD_GROUPS_TBL);
        return $this->id_directory->in($views);
    }
    
    private function getGroupUserIds()
    {
        $userGroups = bab_getUserGroups(bab_getUserId());
        if(isset($userGroups['id'])){
            $users = bab_getGroupsMembers($userGroups['id']);
        }
        $ids = array();
        foreach ($users as $user){
            if(!in_array($user[id], $ids)){
                $ids[] = $user['id'];
            }
        }
        return $ids;
    }
}

/**
 * directorymanager_DirectoryEntry
 *
 * @property string     $cn
 * @property string     $sn
 * @property string     $mn
 * @property string     $givenname
 * @property string     $jpegphoto
 * @property string     $email
 * @property string     $btel
 * @property string     $mobile
 * @property string     $htel
 * @property string     $bfax
 * @property string     $title
 * @property string     $departmentnumber
 * @property string     $organisationname
 * @property string     $bstreetaddress
 * @property string     $bcity
 * @property string     $bpostalcode
 * @property string     $bstate
 * @property string     $bcountry
 * @property string     $hstreetaddress
 * @property string     $hcity
 * @property string     $hpostalcode
 * @property string     $hstate
 * @property string     $hcountry
 * @property string     $user1
 * @property string     $user2
 * @property string     $user3
 * @property string     $photo_data
 * @property string     $photo_type
 * @property int        $id_directory
 * @property int        $id_user
 * @property datetime   $date_modification
 * @property int        $id_modifiedby
 * @property directorymanager_DirectoryEntryExtra[]   $extrafields
 */
class directorymanager_DirectoryEntry extends ORM_Record
{    
    public function getFullName()
    {
        return $this->givenname.' '.$this->sn;
    }
    
    public function getDirectoryFields()
    {
        require_once $GLOBALS['babInstallPath'].'utilit/dirincl.php';
        $fields = bab_getDirectoriesFields(array($this->id_directory));
        ksort($fields);
        return $fields;
    }
    
    public function getExtraFields()
    {
        $extraFields = array();
        foreach ($this->extrafields as $extraField) {
            $extraFields[] = $extraField;
        }
        
        return $extraFields;
    }
    
    /**
     * @return directorymanager_Directory
     */
    public function directory()
    {
        $App = directorymanager_App();
        $set = $App->DirectorySet();
        
        return $set->get($set->id->is($this->id_directory));
    }
    
    public function isUpdatable(){
        $updates = bab_getUserIdObjects(BAB_DBDIRUPDATE_GROUPS_TBL);//Directories updatable by the current user
        
        if(in_array($this->id_directory, $updates)){ //No need to go further if the entry has a directory
            return true;
        }
        
        //It is possible that the entry does not have an associated directory (the entry is associated by the group)
        //In that case, we check if the entry's user is part of a group associated to a directory the current user can update
        $App = directorymanager_App();
        $set = $App->DirectorySet();
        $userGroups = bab_getUserGroups($this->id_user);
        if(isset($userGroups['id'])){
            foreach ($updates as $update){
                $dir = $set->get($set->id->is($update));
                if($dir && in_array($dir->id_group, $userGroups['id'])){
                    //The entry's user is part of a group associated to a directory the current user can update
                    return true;
                }
            }
        }
        
        return false;
    }
    
    public function isCreatable(){
        $creates = bab_getUserIdObjects(BAB_DBDIRADD_GROUPS_TBL);//Directories addable by the current user
        
        if(in_array($this->id_directory, $creates)){ //No need to go further if the entry has a directory
            return true;
        }
        
        //It is possible that the entry does not have an associated directory (the entry is associated by the group)
        //In that case, we check if the entry's user is part of a group associated to a directory the current user can add
        $App = directorymanager_App();
        $set = $App->DirectorySet();
        $userGroups = bab_getUserGroups($this->id_user);
        if(isset($userGroups['id'])){
            foreach ($creates as $create){
                $dir = $set->get($set->id->is($create));
                if($dir && in_array($dir->id_group, $userGroups['id'])){
                    //The entry's user is part of a group associated to a directory the current user can add
                    return true;
                }
            }
        }
        
        return false;
    }
    public function isDeletable(){        
        $deletes = bab_getUserIdObjects(BAB_DBDIRDEL_GROUPS_TBL);//Directories deletable by the current user
        
        if(in_array($this->id_directory, $deletes)){ //No need to go further if the entry has a directory
            return true;
        }
        
        //It is possible that the entry does not have an associated directory (the entry is associated by the group)
        //In that case, we check if the entry's user is part of a group associated to a directory the current user can delete
        $App = directorymanager_App();
        $set = $App->DirectorySet();
        $userGroups = bab_getUserGroups($this->id_user);
        if(isset($userGroups['id'])){
            foreach ($deletes as $delete){
                $dir = $set->get($set->id->is($delete));
                if($dir && in_array($dir->id_group, $userGroups['id'])){
                    //The entry's user is part of a group associated to a directory the current user can delete
                    return true;
                }
            }
        }
        
        return false;
    }
    
    /**
     * @see ORM_Record::getValue()
     */
    public function getValue($fieldName)
    {
        //Field is generic
        if(strpos($fieldName, 'babdirf') === false){
            return parent::getValue($fieldName);
        }
        //Field is custom
        $extraFields = $this->getExtraFields();
        foreach ($extraFields as $extraField){
            /* @var $extraField directorymanager_DirectoryEntryExtra */
            if($extraField->babdirid == $fieldName){
                return $extraField->field_value;
            }
        }
        return null;
    }
    
    /**
     * override default record title
     * @return string
     */
    public function getRecordTitle()
    {
        return $this->getFullName();
    }

    
    /**
     * Update the entry with the data from the Ovidentia User. This does not update any extra fields
     * @return boolean
     */
    public function updateWithOvidentiaUserInfos()
    {
        $infos = bab_getUserInfos($this->id_user);
        if($infos){
            foreach ($infos as $field => $value){
                if($this->getParentSet()->fieldExist($field)){
                    $this->$field = $value;
                }
            }
        }
        return $this->save();
    }
    
    public function delete()
    {
        $delete = parent::delete();
        if($delete && $this->id_user > 0){
            $dir = $this->directory();
            if($dir){
                $workspace = $dir->getWorkspace();
                if($workspace){
                    $workspaceGroup = $workspace->getGroupId();
                    if(isset($workspaceGroup)){
                        bab_removeUserFromGroup($this->id_user, $workspaceGroup);
                    }
                }
            }
        }
        return $delete;
    }
    
    public function notifyWorkspaceRegistration(workspace_Workspace $workspace, $withAccountInfos = false, $password = null)
    {
        if($this->id_user && $this->id_user > 0 && $workspace->isReadable($this->id_user)){
            global $babAdminEmail, $babInstallPath;
            include_once $babInstallPath . 'utilit/mailincl.php';
            include_once $babInstallPath . 'admin/acl.php';
            $App = directorymanager_App();
            $Ui = $App->Ui();
            
            $mail = bab_mail();
            if ($mail == false) {
                return;
            }
            
            $userEmail = bab_getUserEmail($this->id_user);
            $userName = bab_getUserName($this->id_user, true);
            
            $title = sprintf(
                directorymanager_translate('You have been invited to workspace %s'),
                $workspace->name
            );
            $App = workspace_App();
            $homePage = $App->Controller()->Page()->display('home', $workspace->id);
            
            if(!$withAccountInfos || !isset($password) || empty($password)){
                $message = sprintf(
                    directorymanager_translate('__NEW_MEMBER_MESSAGE_HTML__'),
                    bab_toHtml($workspace->name),
                    bab_toHtml($GLOBALS['babUrl'] . $homePage->url())
                );
                $messagetxt = sprintf(
                    directorymanager_translate('__NEW_MEMBER_MESSAGE_TXT__'),
                    $workspace->name,
                    $GLOBALS['babUrl'] . $homePage->url()
                );
            }
            else{
                $login = bab_getUserNickname($this->id_user);
                $message = sprintf(
                    directorymanager_translate('__NEW_MEMBER_MESSAGE_ACCOUNTINFOS_HTML__'),
                    bab_toHtml($workspace->name),
                    bab_toHtml($GLOBALS['babUrl'] . $homePage->url()),
                    bab_toHtml($login),
                    bab_toHtml($password)
                );
                $messagetxt = sprintf(
                    directorymanager_translate('__NEW_MEMBER_MESSAGE_ACCOUNTINFOS_TXT__'),
                    $workspace->name,
                    $GLOBALS['babUrl'] . $homePage->url(),
                    $login,
                    $password
                );
            }
            
            $addonInfo = bab_getAddonInfosInstance('directorymanager');
            
            $tpl = $Ui->NotifyWorkspaceRegistration('', $message, $workspace);
            
            $message = $addonInfo->printTemplate($tpl, 'email.html', 'added_to_workspace_html');
            $messagetxt = $addonInfo->printTemplate($tpl, 'email.html', 'added_to_workspace_text');
            
            $message = $mail->mailTemplate($message);
            
            $mail->mailFrom($babAdminEmail, $GLOBALS['babAdminName']);
            $mail->mailSubject($title);
            $mail->mailBody($message, 'html');
            $mail->mailAltBody($messagetxt);
            
            $mail->mailTo($userEmail, $userName);
            
            $result = $mail->send();
            
            if (!$result) {
                return false;
            }
            
            return true;
        }
    }
}