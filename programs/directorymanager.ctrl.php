<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2018 by CANTICO ({@link http://www.cantico.fr})
 */


/* @var $App Func_App_Directorymanager */
$App = bab_functionality::get('App/Directorymanager');


$App->includeRecordController();


class directorymanager_Controller extends app_CtrlRecord
{
    /**
     * {@inheritDoc}
     * @see app_Controller::getControllerTg()
     */
    protected function getControllerTg()
    {
        return 'addon/directorymanager/main';
    }


    /**
     * Get object name to use in URL from the controller classname
     * @param string $classname
     * @return string
     */
    protected function getObjectName($classname)
    {
        $prefix = strlen('directorymanager_Ctrl');
        return strtolower(substr($classname, $prefix));
    }

    /**
     * @return directorymanager_CtrlDirectory
     */
    public function Directory($proxy = true)
    {
        require_once FUNC_DIRECTORYMANAGER_CTRL_PATH.'directory.ctrl.php';
        return $this->App()->ControllerProxy('directorymanager_CtrlDirectory', $proxy);
    }
    
    /**
     * @return directorymanager_CtrlDirectoryEntry
     */
    public function DirectoryEntry($proxy = true)
    {
        require_once FUNC_DIRECTORYMANAGER_CTRL_PATH.'directoryentry.ctrl.php';
        return $this->App()->ControllerProxy('directorymanager_CtrlDirectoryEntry', $proxy);
    }
}
