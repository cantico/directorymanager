<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';

$App = directorymanager_App();
$App->includeController();


/**
 * This controller manages actions that can be performed on directories.
 */
class directorymanager_CtrlDirectoryEntry extends directorymanager_Controller
{
    /**
     *
     * @param string $portletId
     * @throws Exception
     */
    public function directoryEntryManager($portletId = null, $filter = array())
    {
        $W = bab_Widgets();
        
        $page = $W->BabPage();

        $itemMenus = $this->getListItemMenus();
        
        foreach ($itemMenus as $itemMenuId => $itemMenu) {
            $page->addItemMenu($itemMenuId, $itemMenu['label'], $itemMenu['action']);
        }
        
        $filter['id_directory'] = directorymanager_getConf($portletId . '_currentDirectory', 0);
        $filter['keywords'] = directorymanager_getConf($portletId . '_keywords');
        
        $filteredView = $this->filteredView($filter);
        
        
        //SEARCH
        
        $searchForm = $W->Form();
        $searchForm->setName('search');
        $searchForm->setReadOnly();
        $searchForm->setLayout($W->FlowLayout());
        $searchForm->addItem(
            $searchEdit = $W->LineEdit()
                ->setSize(20)
                ->setName('keywords')
                ->addClass('form-control', 'icon', Func_Icons::ACTIONS_EDIT_FIND)
                ->setPlaceHolder(directorymanager_translate('Search...'))
        );
        if (isset($filter['keywords'])) {
            $searchEdit->setValue($filter['keywords']);
        }
        $searchForm->setAjaxAction($this->proxy()->setSearch($portletId), null, 'change');
        
        $page->addItem($searchForm);
        $page->addItem($filteredView);
        
        $page->setReloadAction($this->proxy()->directoryEntryManager($portletId));
        
        return $page;
    }
    
    public function setSearch($portletId = null, $search = null)
    {
        directorymanager_setConf($portletId . '_keywords', isset($search['keywords']) ? $search['keywords'] : null);
        return true;
    }
    
    /**
     * Returns the xxxModelView associated to the RecordSet.
     *
     * @param array|null    $filter
     * @param string        $type
     * @param array|null    $columns    Optional list of columns. array($columnPath] => '1' | '0').
     * @param string|null   $itemId     Widget item id
     * @return app_TableModelView
     */
    protected function modelView($filter = null, $type = null, $columns = null, $itemId = null)
    {
        $App = $this->App();
        $Ui = $App->Ui();
        
        /* @var $recordSet directorymanager_DirectoryEntrySet */
        $recordSet = $this->getRecordSet();
        
        $recordClassname = $this->getRecordClassName();
        
        $itemId = $this->getModelViewDefaultId($itemId);
        
        if (!isset($type)) {
            $type = $this->getFilteredViewType($itemId);
        }
        
        switch ($type) {
            case 'cards':
                $tableviewClassname =  $recordClassname . 'CardsView';
                break;           
            case 'table':
            default:
                $tableviewClassname =  $recordClassname . 'TableView';
                break;
        }
        
        
        /* @var $tableview widget_TableModelView */
        $tableview = $Ui->$tableviewClassname();
        
        $tableview->setRecordController($this);
        
        $tableview->setId($itemId);
        
        $tableview->setRecordSet($recordSet);
        $tableview->addDefaultColumns($recordSet);
        if (isset($filter)) {
            $tableview->setFilterValues($filter);
        }
        
        //FILTER AND SEARCH
        
        $filter = $tableview->getFilterValues();
        
        if (isset($filter['showTotal']) && $filter['showTotal']) {
            $tableview->displaySubTotalRow(true);
        }
        
        $conditions = $tableview->getFilterCriteria($filter);
        
        $conditions = $conditions->_AND_(
            $recordSet->isReadable()
        );
        
        //When a directory is set, we search for every entry that is :
        // - Directly associated to the directory, with a relation in $entry->id_directory
        // - Or indirectly associated to the directory, when the entry's user is member of the directory's group
        
        if (isset($filter['id_directory']) && $filter['id_directory']) {
            $directorySet = $App->DirectorySet();
            $dir = $directorySet->get($directorySet->id->is($filter['id_directory']));
            if($dir){
                $users = bab_getGroupsMembers($dir->id_group);
                $ids = array();
                foreach ($users as $user){
                    $ids[] = $user['id'];
                }
                $conditions = $conditions->_AND_(
                    $recordSet->id_directory->is($filter['id_directory'])
                    ->_OR_(
                        $recordSet->id_user->in($ids)    
                    )
                );
            }
        }
        
        if (isset($filter['keywords']) && !empty($filter['keywords'])) {
            $subConditions = array();
            $fields = $recordSet->getFields();
            foreach ($fields as $fieldName => $dummy){
                $subConditions[] = $recordSet->$fieldName->contains($filter['keywords']);
            }
            $conditions = $conditions->_AND_(
                $recordSet->any($subConditions)
            );
            
        }
        
        $records = $recordSet->select($conditions); 
        $tableview->setDataSource($records);
        
        //ADD EXTRA FIELDS AS SELECTABLE COLUMNS
        if (isset($filter['id_directory']) && $filter['id_directory']) {
            $dirSet = $App->DirectorySet();
            $dir = $dirSet->get($dirSet->id->is($filter['id_directory']));
            if($dir){
                /* @var $dir directorymanager_Directory */
                $extrafields = $dir->getExtraFields();
                foreach ($extrafields as $extrafield){
                    
                    $tableview->addColumn(
                        widget_TableModelViewColumn(
                            $extrafield['id_field'],
                            $extrafield['name']
                        )->setVisible(false)->setSelectable(true)
                    );
                }
            }
        }
        
        //ACTIONS COLUMN
        $tableview->addColumn(
            widget_TableModelViewColumn('_actions_', ' ')
                ->setSelectable(true, $App->translate('[Menu]'))
                ->setExportable(false)
                ->setSortable(false)
                ->addClass('widget-column-thin', 'widget-column-center')
        );
        
        if (isset($columns)) {
            $availableColumns = $tableview->getVisibleColumns();
            $remainingColumns = array();
            foreach ($availableColumns as $availableColumn) {
                $colPath = $availableColumn->getFieldPath();
                if (isset($columns[$colPath]) && $columns[$colPath] == '1') {
                    $remainingColumns[] = $availableColumn;
                }
            }
            $tableview->setColumns($remainingColumns);
        }
        
        $tableview->allowColumnSelection();
        
        return $tableview;
    }
    
    /**
     * Returns a page containing an editor for the record.
     *
     * @param string|null id    A record id or null for a new record editor.
     * @return Widget_Page
     */
    public function edit($id = null, $directory = null, $view = '')
    {
        $W = bab_Widgets();
        $App = directorymanager_App();
        $Ui = $App->Ui();
        
        $recordSet = $App->DirectoryEntrySet();
        
        $page = $W->BabPage();
        $page->addClass('app-page-editor');
        
        $page->setTitle($App->translate($recordSet->getDescription()));
                
        if (isset($id)) {
            $record = $recordSet->request($id);
            if (!$record->isUpdatable()) {
                throw new app_AccessException($App->translate('You do not have access to this page.'));
            }
        } else {
            $directorySet = $App->DirectorySet();
            /* @var $directory directorymanager_Directory */
            $directory = $directorySet->request($directorySet->id->is($directory));
            if (!$recordSet->isCreatable() || !$directory->isAddableByUser()) {
                throw new app_AccessException($App->translate('You do not have access to this page.'));
            }
            /* @var $record directorymanager_DirectoryEntry */
            $record = $recordSet->newRecord();
            $record->id_directory = $directory->id;
        }
        /* @var $editor directorymanager_DirectoryEntryEditor */
        $editor = $Ui->DirectoryEntryEditor($record);
        $editor->setSaveAction($this->proxy()->edit($id, $view));
        $page->addItem($editor);
        
        
        $title = $record->id > 0 ? directorymanager_translate('Edit entry') : directorymanager_translate('Create entry');
        
        $page->setTitle($title);
        
        return $page;
    }
    
    public function save($data = array())
    {
        $this->requireSaveMethod();
        
        global $babBody;
        
        $App = directorymanager_App();
        $entrySet = $App->DirectoryEntrySet();
        $extraSet = $App->DirectoryEntryExtraSet();
        
        if(isset($data['id']) && !empty($data['id'])){
            //This is an update
            /* @var $entry directorymanager_DirectoryEntry */
            $entry = $entrySet->request($entrySet->id->is($data['id']));
            $entry->setValues($data);
        }
        elseif(isset($data['id_directory']) && !empty($data['id_directory'])){
            //This is a creation
            /* @var $entry directorymanager_DirectoryEntry */
            $entry = $entrySet->newRecord();
            $entry->setValues($data);
        }
        else{
            $babBody->addError(directorymanager_translate('Invalid data'));
            return true; 
        }
        
        if(!$entry->isUpdatable()){
            throw new app_AccessException($App->translate('You do not have access to this page.'));
        }
        
        if($entry->save()){
            //Save the generic fields before saving the custom fields
            foreach ($data as $fieldName => $value) {
                //Field is generic
                if(strpos($fieldName, 'babdirf') === false){
                    //Generic fields have already been saved
                    continue;
                }
                
                //Field is custom
                $fieldx = trim($fieldName, 'babdirf');

                $extra = $extraSet->get($extraSet->id_entry->is($entry->id)->_AND_($extraSet->id_fieldx->is($fieldx)));
                //Create an extra field or update the existing one
                if(!isset($extra)){
                    $extra = $extraSet->newRecord();
                    $extra->id_entry = $entry->id;
                    $extra->id_fieldx = $fieldx;
                }
                $extra->field_value = $value;
                
                if(!$extra->save()){
                    $babBody->addError(directorymanager_translate('An error occured while saving the entry extra fields'));
                    return true;
                } 
            }
            $babBody->addMessage(directorymanager_translate('The entry has been saved'));
            return true;
        }
        
        $babBody->addError(directorymanager_translate('An error occured while saving the entry'));
        return true;        
    }
    
    /**
     * @see app_CtrlRecord::delete()
     */
    public function delete($id)
    {
        global $babBody;
        
        $this->requireDeleteMethod();
        
        $App = directorymanager_App();
        $entrySet = $App->DirectoryEntrySet();
        $extraSet = $App->DirectoryEntryExtraSet();
        
        $entry = $entrySet->request($entrySet->id->is($id));
        
        if (!$entry->isDeletable()) {
            throw new app_AccessException('Sorry, You are not allowed to perform this operation');
        }
        
        $extras = $extraSet->select($extraSet->id_entry->is($entry->id));
        
        //Proceed to delete extra fields first
        foreach ($extras as $extra){
            /* @var $extra directorymanager_DirectoryEntryExtra */
            $extra->delete();
        }
        
        if($entry->delete()){
            $babBody->addMessage(directorymanager_translate('The entry has been deleted'));
            return true;
        }
        $babBody->addError(directorymanager_translate('An error occured while deleting the entry'));
        return true;
    }
    
    protected function toolbar(widget_TableModelView $tableview){
        $W = bab_Widgets();
        $toolbar = parent::toolbar($tableview);
        
        $filter = $tableview->getFilterValues();
        
        if(isset($filter['id_directory'])){
            $App = directorymanager_App();
            $set = $App->DirectorySet();
            /* @var $directory directorymanager_Directory */
            $directory = $set->get($set->id->is($filter['id_directory']));
            if($directory && $directory->isAddableByUser()){
                $toolbar->addItem(
                    $W->Link(
                        directorymanager_translate('Create entry'),
                        $this->proxy()->edit(null, $filter['id_directory'])
                    )->addClass('icon', Func_Icons::ACTIONS_DOCUMENT_NEW)
                    ->setAjaxAction()
                );
                
                $workspace = $directory->getWorkspace();
                if($workspace && $workspace->userIsAdministrator())
                {
                    $toolbar->addItem(
                        $W->Link(
                            directorymanager_translate('Add user to the workspace'),
                            $this->proxy()->addUserToWorkspace($filter['id_directory'])
                        )->addClass('icon', Func_Icons::ACTIONS_USER_NEW)->setOpenMode(Widget_Link::OPEN_DIALOG_AND_RELOAD)
                    );
                }
            }
        }
        return $toolbar;
    }
    
    public function createNewUser($directory = null){       
        $W = bab_Widgets();
        
        $view = $this->createNewUserView($directory);
        
        $box = $W->VBoxItems(
            $view
        );
        
        return $box;
    }
    
    public function createNewUserView($directory = null)
    {
        $W = bab_Widgets();
        $W->includePhpClass('Widget_Link');
        $App = directorymanager_App();
        $Ui = $App->Ui();
        $set = $App->DirectoryEntrySet();        
        
        $newUserBox = $W->FlowItems()->addClass('row');
        
        $set = $App->DirectoryEntrySet();
        $record = $set->newRecord();
        $record->id_directory = $directory;
        
        $editor = $Ui->DirectoryEntryEditor($record, false);
        
        $loginFormItem = $W->LabelledWidget(
            $App->translate('User login'),
            $W->LineEdit()
                ->setSize(30)
                ->setAutoComplete(false)
                ->setMandatory(true)
                ->setAjaxValidateAction($this->proxy()->validateNickname()),
            'nickname'
        );
        
        $passwordFormItem = $W->LabelledWidget(
            $App->translate('Password'),
            $W->LineEdit()
                ->setSize(30)
                ->obfuscate(true)
                ->setAutoComplete(false)
                ->setMandatory(true)
            ->setAjaxValidateAction($this->proxy()->validatePassword()),
            'password'
        );
        
        $notify = $W->LabelledWidget(
            $App->translate('Notify the user'),
            $W->CheckBox(),
            'notifyNewUser'
        );
        
        $sendPassword = $W->LabelledWidget(
            $App->translate('Send login and password'),
            $W->CheckBox(),
            'accountInfos'
        );
        
        $notify->setAssociatedDisplayable($sendPassword, array('1'));
        
        $createUser = $W->VBoxItems(
            $loginFormItem,
            $passwordFormItem,
            $notify,
            $sendPassword
        );
        
        $newUserBox->addItems(
            $W->Section(
                directorymanager_translate('Authentication informations'),
                $createUser
            )->setSizePolicy('col-md-3 col-xs-12')->setFoldable(false, false),
            $editor->setSizePolicy('col-md-9 col-xs-12')
        );

        $editor->addButton(
            $W->SubmitButton()
                ->validate(true)
                ->setLabel(directorymanager_translate('Add user'))
                ->setAjaxAction($App->Controller()->DirectoryEntry()->saveNewUserToWorkspace())
        );
        
        return $newUserBox;
    }
    
    public function validateNickname($value = null)
    {
        require_once $GLOBALS['babInstallPath'].'utilit/loginIncl.php';
        
        //The nickname is already used
        if(bab_getUserByNickname($value)){
            $W = bab_Widgets();
            $page = $W->BabPage();
            $page->setEmbedded(false);
            $page->addItem(
                $W->Label(directorymanager_translate('An account with this nickname already exists'))
            );
            return $page;
        }
        
        return true;
    }
    
    public function validatePassword($value = null)
    {
        $oPwdComplexity = @bab_functionality::get('PwdComplexity');
        
        //Password does not comply with complexity policy
        if (!$oPwdComplexity->isValid($value)) {            
            $W = bab_Widgets();
            $page = $W->BabPage();
            $page->setEmbedded(false);
            $page->addItem(
                $W->Label($oPwdComplexity->getErrorDescription())
            );
            return $page;
        }
        return true;
    }
    
    public function validateEmail($value = null)
    {
        //Email does not haev the right format
        if (!bab_isEmailValid($value)) {
            $W = bab_Widgets();
            $page = $W->BabPage();
            $page->setEmbedded(false);
            $page->addItem(
                $W->Label(directorymanager_translate('Invalid email address'))    
            );
            return $page;
        }
        return true;
    }
    
    public function addUserToWorkspace($directory = null){
        $babBody = $GLOBALS['babBody'];
        
        $App = directorymanager_App();
        $set = $App->DirectorySet();
        
        /* @var $directory directorymanager_Directory */
        $directory = $set->request($set->id->is($directory));
        /* @var $workspace workspace_Workspace */
        $workspace = $directory->getWorkspace();
        
        //No workspace
        if(!isset($workspace)){
            $babBody->addError(directorymanager_translate('This directory is not associated to a workspace'));
            return true;
        }
        
        //User is not workspace admin
        if(!$workspace->userIsAdministrator()){
            $babBody->addError(directorymanager_translate('Access denied'));
            return true;
        }
        
        $Ui = $App->Ui();
        $page = $Ui->Page();
        
        $page->setTitle(directorymanager_translate('Add user to the workspace'));
        
        $recordSet = $App->DirectoryEntrySet();
        $record = $recordSet->newRecord();
        $record->id_directory = $directory->id;
        
        $editor = $Ui->DirectoryEntryAddUserToWorkspace($record);
        $page->addItem($editor);
        $page->setReloadAction($this->proxy()->addUserToWorkspace($directory->id));
        
        return $page;
    }
    
    
    
    public function saveUserToWorkspace($data = null)
    {
        $babBody = $GLOBALS['babBody'];
        
        $App = directorymanager_App();
        $set = $App->DirectoryEntrySet();
        
        //Incomplete data
        if(!isset($data['id_directory']) || !isset($data['existingUser']) || empty($data['existingUser'])){
            $babBody->addError(directorymanager_translate('Invalid data'));
            return true;
        }
        
        $user = bab_getUserInfos($data['existingUser']);
        if(!$user){
            $babBody->addError(directorymanager_translate('User was not found'));
            return true;
        }
        
        $dirSet = $App->DirectorySet();
        /* @var $dir directorymanager_Directory */
        $dir = $dirSet->get($dirSet->id->is($data['id_directory']));
        
        //Directory not found
        if(!isset($dir)){
            $babBody->addError(directorymanager_translate('The directory was not found'));
            return true;
        }
        
        /* @var $workspace workspace_Workspace */
        $workspace = $dir->getWorkspace();
        //No workspace
        if(!isset($workspace)){
            $babBody->addError(directorymanager_translate('The directory is not associated to a workspace'));
            return true;
        }
        
        //No rights
        if(!$workspace->userIsAdministrator() || !$dir->isAddableByUser()){
            $babBody->addError(directorymanager_translate('Access denied'));
            return true;
        }
        
        $workspaceGroupId = $workspace->getGroupId();
        //No group
        if(!isset($workspaceGroupId)){
            $babBody->addError(directorymanager_translate('There is no group associated to the workspace'));
            return true;
        }
        
        $record = $set->newRecord();
        $record->id_directory = $data['id_directory'];
        $record->id_user = $data['existingUser'];
        
        //Error on save entry
        if(!$record->save()){
            $babBody->addError(directorymanager_translate('An error occured while saving the entry'));
            return true;
        }
        
        //Error on update entry data
        if(!$record->updateWithOvidentiaUserInfos()){
            $babBody->addError(directorymanager_translate('An error occured while saving the entry'));
            return true;
        }
        
        
        //Add user to workspace group
        bab_addUserToGroup($data['existingUser'], $workspaceGroupId);
        
        //Notify user
        if(isset($data['notifyUser']) && $data['notifyUser'] == 1){
            $record->notifyWorkspaceRegistration($workspace);
        }
        
        $babBody->addMessage(directorymanager_translate('The user has been added to the workspace'));
        return true;
    }
    
    public function saveNewUserToWorkspace($data = null)
    {
        require_once $GLOBALS['babInstallPath'].'utilit/userincl.php'; //Used for bab_addUser()
        require_once $GLOBALS['babInstallPath'].'utilit/loginIncl.php'; //Used for bab_getUserByNickname()
        $babBody = $GLOBALS['babBody'];
        
        $App = directorymanager_App();
        
        $userInfos = $data['data'];
        
        //Incomplete data
        if(!isset($data['id_directory']) || !isset($data['nickname']) || !isset($data['password']) || !isset($userInfos['givenname']) || !isset($userInfos['sn']) || !isset($userInfos['email']) || empty($data['id_directory']) || empty($data['nickname']) || empty($data['password']) || empty($userInfos['givenname']) || !isset($userInfos['sn']) || empty($userInfos['email'])){
            $babBody->addError(directorymanager_translate('Invalid data'));
            return true;
        }
        
        if(!bab_isEmailValid($userInfos['email'])){
            $babBody->addError(directorymanager_translate('Invalid email address'));
            return true;
        }
        
        $dirSet = $App->DirectorySet();
        /* @var $dir directorymanager_Directory */
        $dir = $dirSet->get($dirSet->id->is($data['id_directory']));
        
        //Directory not found
        if(!isset($dir)){
            $babBody->addError(directorymanager_translate('The directory was not found'));
            return true;
        }
        
        /* @var $workspace workspace_Workspace */
        $workspace = $dir->getWorkspace();
        //No workspace
        if(!isset($workspace)){
            $babBody->addError(directorymanager_translate('The directory is not associated to a workspace'));
            return true;
        }
        
        //No rights
        if(!$workspace->userIsAdministrator() || !$dir->isAddableByUser()){
            $babBody->addError(directorymanager_translate('Access denied'));
            return true;
        }
        
        $workspaceGroupId = $workspace->getGroupId();
        //No group
        if(!isset($workspaceGroupId)){
            $babBody->addError(directorymanager_translate('There is no group associated to the workspace'));
            return true;
        }
        
        //The nickname is already used
        if(bab_getUserByNickname($data['nickname'])){
            $babBody->addError(directorymanager_translate('An account with this nickname already exists'));
            return true;
        }
                
        //Try to create a new user
        $error = '';
        $userId = bab_addUser($userInfos['givenname'], $userInfos['sn'], '', $userInfos['email'], $data['nickname'], $data['password'], $data['password'], true, $error);
        
        //Error or user creation
        if(strlen($error) > 0){
            $babBody->addError($error);
            return true;
        }
        
        //bab_addUser() did not return a user id, meaning there is an uncatched error
        if($userId === false){
            $babBody->addError(directorymanager_translate('An error occured while creating the user account'));
        }
        
        //All checks have been done, proceed to entry creation

        $dirEntrySet = $App->DirectoryEntrySet();
        
        /* @var $entry directorymanager_DirectoryEntry */
        $entry = $dirEntrySet->newRecord();
        
        $entry->setValues($userInfos);
        $entry->id_directory = $dir->id;
        $entry->id_user = $userId;
        
        //Error on save entry
        if(!$entry->save()){
            $babBody->addError(directorymanager_translate('An error occured while saving the entry'));
            return true;
        }
        
        //Check for extrafield, $entry->setValues() has already set the values for the generic fields
        $extraFields = $dir->getExtraFields();
        $extraFieldsId = array();
        foreach ($extraFields as $extraField){
            $extraFieldsId[] = $extraField['id_field'];
        }
        
        $extraSet = $App->DirectoryEntryExtraSet();
        
        if(count($extraFieldsId) > 0){
            //Only parse extra fields if we know the directory has at least one
            foreach ($userInfos as $fieldName => $field_value) {
                if(in_array($fieldName, $extraFieldsId)){
                    //$fieldName is an extra field
                    $id_fieldx = trim('babdirf', $fieldName);
                    //Create an extra field or update the existing one
                    $extra = $extraSet->get($extraSet->id_fieldx->is($id_fieldx)->_AND_($extraSet->id_entry->is($entry->id)));
                    if(!$extra){
                        $extra = $extraSet->newRecord();
                        $extra->id_fieldx = $id_fieldx;
                        $extra->id_entry = $entry->id;
                    }
                    $extra->field_value = $field_value;
                    $extra->save();
                }
            }
        }
        
        //Add user to workspace group
        bab_addUserToGroup($userId, $workspaceGroupId);
        
        //Notify user
        if(isset($data['notifyNewUser']) && $data['notifyNewUser'] == 1){
            $sendInfos = isset($data['accountInfos']) && $data['accountInfos'] == 1 ? true : false;
            $password = $sendInfos ? $data['password'] : null;
            $entry->notifyWorkspaceRegistration($workspace, $sendInfos, $password);
        }
        
        $babBody->addMessage(directorymanager_translate('The user has been added to the workspace'));
        return true;
    }
}
;