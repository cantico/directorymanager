<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';

$addon_directorymanager = bab_getAddonInfosInstance('directorymanager');

if (!$addon_directorymanager) {
    return;
}

define('FUNC_DIRECTORYMANAGER_PHP_PATH', $addon_directorymanager->getPhpPath());
define('FUNC_DIRECTORYMANAGER_CTRL_PATH', FUNC_DIRECTORYMANAGER_PHP_PATH.'ctrl/');
define('FUNC_DIRECTORYMANAGER_SET_PATH', FUNC_DIRECTORYMANAGER_PHP_PATH.'set/');
define('FUNC_DIRECTORYMANAGER_UI_PATH', FUNC_DIRECTORYMANAGER_PHP_PATH.'ui/');
bab_functionality::includeFile('App');




/**
 * Func_App_Directorymanager
 */
class Func_App_Directorymanager extends Func_App
{

    public function __construct()
    {
        parent::__construct();

        $this->addonPrefix = 'directorymanager';
        $this->addonName = 'directorymanager';
        $this->classPrefix = $this->addonPrefix . '_';
        $this->controllerTg = 'addon/directorymanager/main';
    }


    /**
     * @return string
     *
     */
    public function getDescription()
    {
        return 'The Ovidentia Directory Manager';
    }







    function setTranslateLanguage($language)
    {
        parent::setTranslateLanguage($language);
        $translate = bab_functionality::get('Translate/Gettext');
        /* @var $translate Func_Translate_Gettext */
        $translate->setLanguage($language);
    }


    /**
     * Translates the string.
     *
     * @param string $str
     * @return string
     */
    function translate($str, $str_plurals = null, $number = null)
    {
        require_once FUNC_DIRECTORYMANAGER_PHP_PATH . 'functions.php';
        $translation = $str;
        if ($translate = bab_functionality::get('Translate/Gettext')) {
            /* @var $translate Func_Translate_Gettext */
            $translate->setAddonName('directorymanager');
            $translation = $translate->translate($str, $str_plurals, $number);
        }
        if ($translation === $str) {
            $translation = parent::translate($str, $str_plurals, $number);
        }

        return $translation;
    }


    /**
     * Includes Controller class definition.
     */
    public function includeController()
    {
        parent::includeController();
        require_once FUNC_DIRECTORYMANAGER_PHP_PATH . 'directorymanager.ctrl.php';
    }

    /**
     *
     * @return directorymanager_Controller
     */
    function Controller()
    {
        self::includeController();
        return new directorymanager_Controller($this);
    }


    /**
     * Include class directorymanager_Access
     */
    protected function includeAccess()
    {
        parent::includeAccess();
        require_once FUNC_DIRECTORYMANAGER_PHP_PATH . '/access.class.php';
    }



    /**
     * Include class directorymanager_Ui
     */
    public function includeUi()
    {
        parent::includeUi();
        require_once FUNC_DIRECTORYMANAGER_UI_PATH . '/ui.class.php';
    }

    /**
     * The directorymanager_Ui object propose an access to all ui files and ui objects (widgets)
     *
     * @return directorymanager_Ui
     */
    public function Ui()
    {
        $this->includeUi();
        return bab_getInstance('directorymanager_Ui');
    }
    
    /**
     * SET DECLARATION
     */
    
    //Directory Set
    
    public function includeDirectorySet()
    {
        require_once FUNC_DIRECTORYMANAGER_SET_PATH.'directory.class.php';
    }
    
    public function DirectoryClassName()
    {
        return 'directorymanager_Directory';
    }
    
    public function DirectorySetClassName()
    {
        return $this->DirectoryClassName() . 'Set';
    }
    
    /**
     *
     * @return directorymanager_DirectorySet
     */
    public function DirectorySet()
    {
        $this->includeDirectorySet();
        $className = $this->DirectorySetClassName();
        $set = new $className($this);
        return $set;
    }
    
    //DirectoryEntry Set
    
    public function includeDirectoryEntrySet()
    {
        require_once FUNC_DIRECTORYMANAGER_SET_PATH.'directoryentry.class.php';
    }
    
    public function DirectoryEntryClassName()
    {
        return 'directorymanager_DirectoryEntry';
    }
    
    public function DirectoryEntrySetClassName()
    {
        return $this->DirectoryEntryClassName() . 'Set';
    }
    
    /**
     *
     * @return directorymanager_DirectoryEntrySet
     */
    public function DirectoryEntrySet()
    {
        $this->includeDirectoryEntrySet();
        $className = $this->DirectoryEntrySetClassName();
        $set = new $className($this);
        return $set;
    }
    
    //DirectoryEntryExtra Set
    
    public function includeDirectoryEntryExtraSet()
    {
        require_once FUNC_DIRECTORYMANAGER_SET_PATH.'directoryentryextra.class.php';
    }
    
    public function DirectoryEntryExtraClassName()
    {
        return 'directorymanager_DirectoryEntryExtra';
    }
    
    public function DirectoryEntryExtraSetClassName()
    {
        return $this->DirectoryEntryExtraClassName() . 'Set';
    }
    
    /**
     *
     * @return directorymanager_DirectoryEntryExtraSet
     */
    public function DirectoryEntryExtraSet()
    {
        $this->includeDirectoryEntryExtraSet();
        $className = $this->DirectoryEntryExtraSetClassName();
        $set = new $className($this);
        return $set;
    }
}
