<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2012 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';



require_once dirname(__FILE__) . '/functions.php';

bab_Functionality::includefile('PortletBackend');



$GLOBALS['Func_PortletBackend_DirectoryManager_Categories'] = array(
    'portal_tools'	=> directorymanager_translate('Portal tools')
);


/**
 * Portlet backend
 */
class Func_PortletBackend_DirectoryManager extends Func_PortletBackend
{


    public function getDescription()
    {
        return directorymanager_translate('Directory Management');
    }
    
    public function select($category = null)
    {
        $addon = bab_getAddonInfosInstance('directorymanager');
        if (!$addon || !$addon->isAccessValid()) {
            return array();
        }
        
        global $Func_PortletBackend_DirectoryManager_Categories;
        
        if (empty($category) || in_array($category, $Func_PortletBackend_DirectoryManager_Categories)) {
            return array(
                'DirectoryManager' => $this->portlet_DirectoryManager(),
            );
        }
        
        return array();
    }

    public function portlet_DirectoryManager()
    {
        return new directorymanager_PortletDefinition_DirectoryManager();
    }

    
    /**
     * get a list of categories supported by the backend
     * @return Array
     */
    public function getCategories()
    {
        global $Func_PortletBackend_DirectoryManager_Categories;
        
        return $Func_PortletBackend_DirectoryManager_Categories;
        
    }
    
    /**
     * (non-PHPdoc)
     * @see Func_PortletBackend::getConfigurationActions()
     */
    public function getConfigurationActions()
    {
        return array();
    }
}







////////////////////////////////////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////////////////////////////////////


class directorymanager_PortletDefinition_DirectoryManager implements portlet_PortletDefinitionInterface
{
    /**
     * @var bab_addonInfos $addon
     */
    protected $addon;

    public function __construct()
    {
        $this->addon = bab_getAddonInfosInstance('directorymanager');
    }


    public function getId()
    {
        return 'DirectoryManager';
    }


    public function getName()
    {
        return directorymanager_translate('Directory Manager');
    }


    public function getDescription()
    {
        return directorymanager_translate('A simple directory manager.');
    }


    public function getPortlet()
    {
        return new directorymanager_Portlet_DirectoryManager();
    }


    /**
     * @return array
     */
    public function getPreferenceFields()
    {
        $userDirectories = bab_getUserDirectories();
        
        $options = array();
        foreach ($userDirectories as $userDirectory){
            $options[] = array(
                'value' => $userDirectory['id'],
                'label' => $userDirectory['name']
            );
        }
        
        return array(
            array(
                'name'      => 'directory',
                'label'     => directorymanager_translate('Directory to display'),
                'type'      => 'list',
                'options'   => $options
            ),
            array(
                'type' => 'list',
                'name' => 'displayType',
                'label' => directorymanager_translate('Display type'),
                'options' => array(
                    array(
                        'label' => directorymanager_translate('Details'),
                        'value' => 'details'
                    ),
                    array(
                        'label' => directorymanager_translate('Cards'),
                        'value' => 'cards'
                    )
                )
            )
        );
    }

    /**
     * Returns the widget rich icon URL.
     * 128x128 ?
     *
     * @return string
     */
    public function getRichIcon()
    {
        return $this->addon->getImagesPath() . 'icon48.png';
    }


    /**
     * Returns the widget icon URL.
     * 16x16 ?
     *
     * @return string
     */
    public function getIcon()
    {
        return $this->addon->getImagesPath() . 'icon48.png';
    }

    /**
     * Get thumbnail URL
     * max 120x60
     */
    public function getThumbnail()
    {
        return $this->addon->getImagesPath() . 'thumbnail.png';
    }

    public function getConfigurationActions()
    {
        return array();
    }
}


$W = bab_Widgets();
$W->Frame();

class directorymanager_Portlet_DirectoryManager extends widget_Frame implements portlet_PortletInterface
{
    
    private $id;
    
    private $configuration;
    
    private $directory;
    
    private $displayType;

    /**
     * {@inheritDoc}
     * @see Widget_Widget::getName()
     */
    public function getName()
    {
        return '';
    }

    /**
     * {@inheritDoc}
     * @see portlet_PortletInterface::getPortletDefinition()
     */
    public function getPortletDefinition()
    {
        return new directorymanager_PortletDefinition_DirectoryManager();
    }

    /**
     * @param string $key
     * @param mixed $defaultValue
     * @return mixed
     */
    private function getConfiguration($key, $defaultValue)
    {
        if (isset($this->configuration[$key])) {
            return $this->configuration[$key];
        }
        return $defaultValue;
    }


    /**
     * {@inheritDoc}
     * @see portlet_PortletInterface::setPreferences()
     */
    public function setPreferences(array $configuration)
    {
        $this->configuration = $configuration;

        $this->directory = $this->getConfiguration('directory', null);
        $this->displayType = $this->getConfiguration('displayType', 'details');
    }


    /**
     * {@inheritDoc}
     * @see Widget_Frame::display()
     */
    public function display(Widget_Canvas $canvas)
    {
        $App = directorymanager_App();
        $addon = bab_getAddonInfosInstance('directorymanager');
        if (!$addon->isAccessValid()) {
            bab_debug('directorymanager addon is not accessible, portlet is displayed empty');
            return '';
        }
        
        directorymanager_setConf($this->id . '_currentDirectory', $this->directory);
        directorymanager_setConf($this->id . '_displayType', $this->displayType);
        $this->setId('directorymanager_' . $this->id); // The widget item id.
        
        $this->setLayout($App->Controller()->DirectoryEntry(false)->directoryEntryManager($this->id));

        $directorymanagerAddon = bab_getAddonInfosInstance('directorymanager');
        
        $display = parent::display($canvas);
        $display .= $canvas->loadStyleSheet($directorymanagerAddon->getStylePath() . 'directorymanager.css');

        return $display;
    }


    public function setPreference($name, $value)
    {

    }

    public function setPortletId($id)
    {
        $this->id = $id;
    }
}
