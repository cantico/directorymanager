<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2018 by CANTICO ({@link http://www.cantico.fr})
 */
require_once dirname(__FILE__).'/functions.php';

/**
 *
 */
class Func_WorkspaceAddon_DirectoryManager extends Func_WorkspaceAddon implements workspace_Configurable
{
    public function getDescription()
    {
        return directorymanager_translate('Allow Worskpace addon to use DirectoryManager');
    }
    
    public function getName()
    {
        return directorymanager_translate('Directories');
    }
    
    public function getConfigurationEditor($workspaceId)
    {
        $W = bab_Widgets();

        $editor = parent::getConfigurationEditor($workspaceId);
        $editor->addItem(
            $W->LabelledWidget(
                directorymanager_translate('Update the directory entry when the portal entry is updated'), 
                $W->CheckBox()->setName('synchronizeEntries')             
            )
        );
        
        $editor->addItem(
            $W->LabelledWidget(
                directorymanager_translate('Managers can create user accounts'),
                $W->CheckBox()->setName('allowAccountCreation')
            )
        );

        return $editor;
    }
    
    public function getIconClassName()
    {
        return Func_Icons::APPS_DIRECTORIES;
    }
    
    public function applyConfiguration($workspaceId)
    {
        /* @var $workspaceApi Func_App_Workspace */
        $workspaceApi = bab_Functionality::get('App/Workspace');
        if($workspaceApi){
            $workspaceSet = $workspaceApi->WorkspaceSet();
            /* @var $workspace workspace_Workspace */
            $workspace = $workspaceSet->request($workspaceSet->id->is($workspaceId));
            if($workspace->delegation == 0 || $workspace->name == ''){
                return;
            }
            $configuration = $this->getConfiguration($workspace->id);
            
            $directoryApi = directorymanager_App();
            $directorySet = $directoryApi->DirectorySet();
            
            /* @var $workspaceDirectory directorymanager_Directory */
            $workspaceDirectory = $directorySet->get($directorySet->id_dgowner->is($workspace->delegation));
            if($workspaceDirectory){
                
            }
            else{
                $this->initializeDirectory($workspace, $configuration);
            }
            
        }
    }
    
    public function getPortletDefinitionId()
    {
        /* @var $portletFunc Func_PortletBackend_DirectoryManager */
        $portletFunc = bab_Functionality::get('PortletBackend/DirectoryManager');
        if($portletFunc){
            /* @var $portlet directorymanager_PortletDefinition_DirectoryManager */
            $portlet = $portletFunc->portlet_DirectoryManager();
            return $portlet->getId();
        }
        return false;
    }
    
    public function getPortletConfiguration($workspaceId)
    {
        $portletConfiguration = array(
            '_blockTitleType' => 'custom',
            '_blockTitle' => '',
            'displayType' => 'details'
        );
        /* @var $workspaceApi Func_App_Workspace */
        $workspaceApi = bab_Functionality::get('App/Workspace');
        if($workspaceApi){
            $workspaceSet = $workspaceApi->WorkspaceSet();
            /* @var $workspace workspace_Workspace */
            $workspace = $workspaceSet->request($workspaceSet->id->is($workspaceId));
            if($workspace->delegation == 0 || $workspace->name == ''){
                return $portletConfiguration;
            }
            
            $directoryApi = directorymanager_App();
            $directorySet = $directoryApi->DirectorySet();
            
            /* @var $directory directorymanager_Directory */
            $directory = $directorySet->get($directorySet->id_dgowner->is($workspace->delegation));
            if($directory){
                $portletConfiguration['directory'] = $directory->id;
            }
        }
        return $portletConfiguration;
    }
    
    private function initializeDirectory(workspace_Workspace $workspace, $configuration = array())
    {
        require_once $GLOBALS['babInstallPath'] . 'admin/acl.php';
        
        global $babDB;
        
        $App = directorymanager_App();
        $set = $App->DirectorySet();
        
        if (empty($workspace->name)) {
            throw new Exception('The directory name cannot be empty.');
        }
        
        $dir = $set->select($set->name->is($workspace->name));
        if(count($dir) > 0){
            throw new Exception('A directory with this name already exists.');
        }
        
        /* @var $dir directorymanager_Directory */
        $dir = $set->newRecord();
        $dir->name = $workspace->name;
        $dir->description = $workspace->description;
        $dir->show_update_info = 'N';
        $dir->id_dgowner = $workspace->delegation;
        $dir->id_group = $workspace->group;
        
        $dir->save();
        
        $res = $babDB->db_query('SELECT * FROM ' . BAB_DBDIR_FIELDS_TBL);
        $k = 0;
        while ($arr = $babDB->db_fetch_array($res)) {
            $modifiable = 'N';
            $required = 'N';
            $multiline = 'N';
            $disabled = 'N';
            
            switch ($arr['name']) {
                case 'givenname':
                    $ordering = 1; break;
                case 'sn':
                    $ordering = 2; break;
                default:
                    $ordering = 0; break;
            }
            
            $req = 'INSERT INTO ' . BAB_DBDIR_FIELDSEXTRA_TBL . ' (id_directory, id_field, default_value, modifiable, required, multilignes, disabled, ordering, list_ordering) VALUES (' .$babDB->quote($dir->id). ', ' . $babDB->quote($arr['id']). ', 0, ' . $babDB->quote($modifiable). ', ' . $babDB->quote($required). ', ' . $babDB->quote($multiline). ', ' . $babDB->quote($disabled) . ', ' . $babDB->quote($ordering) . ', ' . ($k++) .')';
            $babDB->db_query($req);
        }
        
        $readersRightsTables = array(
            BAB_DBDIRVIEW_GROUPS_TBL,
            BAB_DBDIREXPORT_GROUPS_TBL
        );
        $writersRightsTables = array(
            BAB_DBDIRVIEW_GROUPS_TBL,
            BAB_DBDIREXPORT_GROUPS_TBL
        );
        $administratorsRightsTables = array(
            BAB_DBDIRVIEW_GROUPS_TBL,
            BAB_DBDIRADD_GROUPS_TBL,
            BAB_DBDIRUPDATE_GROUPS_TBL,
            BAB_DBDIRDEL_GROUPS_TBL,
            BAB_DBDIREXPORT_GROUPS_TBL,
            BAB_DBDIRIMPORT_GROUPS_TBL,
            BAB_DBDIRBIND_GROUPS_TBL,
            BAB_DBDIRUNBIND_GROUPS_TBL,
            BAB_DBDIREMPTY_GROUPS_TBL
        );
        
        //  Groups
        //  The readers group is groupId
        $readersGroupId = $workspace->getDelegationGroupId();
        $writersGroupId = $workspace->getWritersGroupId();
        $administratorsGroupId = $workspace->getAdministratorsGroupId();
        
        foreach ($readersRightsTables as $rightsTable) {
            aclAdd($rightsTable, $readersGroupId, $dir->id);
        }
        foreach ($writersRightsTables as $rightsTable) {
            aclAdd($rightsTable, $writersGroupId, $dir->id);
        }
        foreach ($administratorsRightsTables as $rightsTable) {
            aclAdd($rightsTable, $administratorsGroupId, $dir->id);
        }
        
        $babDB->db_query("update ".BAB_GROUPS_TBL." set directory='Y' where id='".$babDB->db_escape_string($workspace->group)."'");
        
        return $dir->id;
    }

    public function setDefaultConfiguration($workspaceId)
    {
        /* @var $workspaceApi Func_App_Workspace */
        $workspaceApi = bab_Functionality::get('App/Workspace');
        if($workspaceApi){
            $workspaceSet = $workspaceApi->WorkspaceSet();
            /* @var $workspace workspace_Workspace */
            $workspace = $workspaceSet->request($workspaceSet->id->is($workspaceId));
            bab_Registry::set('/workspace/workspaces/'.$workspace->delegation.'/addons/DirectoryManager/isWorkspaceEnabled', 1);
            bab_Registry::set('/workspace/workspaces/'.$workspace->delegation.'/addons/DirectoryManager/allowAccountCreation ', 0);
            bab_Registry::set('/workspace/workspaces/'.$workspace->delegation.'/addons/DirectoryManager/synchronizeEntries ', 0);
        }
    }
}