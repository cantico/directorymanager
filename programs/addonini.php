; <?php/*

[general]
name							="directorymanager"
version							="0.0.1"
encoding						="UTF-8"
description						="Dictory manager"
description.fr					="Gestionnaire d'annuaires"
delete							=1
db_prefix						="directorymanager_"
ov_version						="8.6.97"
php_version						="5.4.0"
mysql_version					="5.1"
addon_access_control			="1"
author							="Robin Bailleux (robin.bailleux@cantico.fr)"
icon							="icon.png"
mysql_character_set_database	="latin1,utf8"
license                         ="GPL-2.0+"
tags                            ="extension,directory"

[addons]
libapp                          ="0.0.1"
LibOrm							="0.9.12"
widgets							="1.0.90"
LibFileManagement				="0.2.23"
portlets                        ="0.3"

;*/?>