<?php
// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';

function directorymanager_translate($str, $str_plurals = null, $number = null)
{
    if ($translate = bab_functionality::get('Translate/Gettext')) {
        /* @var $translate Func_Translate_Gettext */
        $translate->setAddonName('directorymanager');

        return $translate->translate($str, $str_plurals, $number);
    }

    return $str;
}

/**
 * Instanciates the App factory.
 *
 * @return Func_App_Directorymanager
 */
function directorymanager_App()
{
    return bab_Functionality::get('App/Directorymanager');
}

/**
 * Returns the value set in the user session for the specified $key
 * @param string    $key
 * @param mixed     $defaultValue
 * @return mixed|NULL
 */
function directorymanager_getConf($key, $defaultValue = null)
{
    if (isset($_SESSION['directorymanager_' . $key])) {
        return $_SESSION['directorymanager_' . $key];
    }
    return null;
}

/**
 * Set $value in the user session for the specified $key
 * @param string    $key
 * @param mixed     $value
 */
function directorymanager_setConf($key, $value = null)
{
    $_SESSION['directorymanager_' . $key] = $value;
}