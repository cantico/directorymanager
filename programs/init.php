<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
 ************************************************************************/
require_once 'base.php';
require_once dirname(__FILE__) . '/functions.php';

/**
 * Fonction appellée au moment de la mise à jour et de l'installation de l'addon
 */
function directorymanager_upgrade($version_base, $version_ini)
{
    require_once dirname(__FILE__) . '/directorymanager.php';
    
    $functionalities = new bab_functionalities();
    
    
    $addon = bab_getAddonInfosInstance('directorymanager');
    $addonPhpPath = $addon->getPhpPath();
    
    if ($functionalities->registerClass('Func_App_Directorymanager', $addonPhpPath . 'directorymanager.php')) {
        echo(bab_toHtml('Functionality "Func_App_Directorymanager" registered.'));
    }
    
    @bab_functionality::includefile('PortletBackend');
    
    if (class_exists('Func_PortletBackend')) {
        $addonPhpPath = $addon->getPhpPath();
        require_once dirname(__FILE__) . '/portletbackend.class.php';
        $functionalities->registerClass('Func_PortletBackend_DirectoryManager', $addonPhpPath . 'portletbackend.class.php');
    }
    
    directorymanager_instanciateWorkspaceAddon();
	
    $addon->addEventListener('bab_eventUserModified', 'directorymanager_onUserModified', 'init.php');
    $addon->addEventListener('bab_eventAddonUpgraded', 'directorymanager_onAddonUpgraded', 'init.php');
    return true;
}

/**
 * Fonction appellée au moment de la suppression de l'addon
 */
function directorymanager_onDeleteAddon()
{
    require_once $GLOBALS['babInstallPath'].'utilit/functionalityincl.php';
    $functionalities = new bab_functionalities();
    $functionalities->unregister('PortletBackend/DirectoryManager');
    $functionalities->unregister('WorkspaceAddon/DirectoryManager');
    $functionalities->unregister('App/Directorymanager');
    
    $addon = bab_getAddonInfosInstance('directorymanager');
    $addon->removeAllEventListeners();
    
    return true;
}

function directorymanager_instanciateWorkspaceAddon()
{
    @bab_functionality::includefile('WorkspaceAddon');
    
    $functionalities = new bab_functionalities();
    $addon = bab_getAddonInfosInstance('directorymanager');
    
    if (class_exists('Func_WorkspaceAddon')) {
        $addonPhpPath = $addon->getPhpPath();
        require_once dirname(__FILE__) . '/workspaceaddon.class.php';
        $functionalities->registerClass('Func_WorkspaceAddon_DirectoryManager', $addonPhpPath . 'workspaceaddon.class.php');
    }
}

/**
 * When the infos of a Ovidentia user is modified, this function relies on the WorkspaceAddon functionality to determine if a directory entry has to be updated or not
 * @param bab_eventUserModified $event
 * @return void
 */
function directorymanager_onUserModified(bab_eventUserModified $event)
{
    $App = directorymanager_App();
    $addonFunc = bab_functionality::get('WorkspaceAddon/DirectoryManager');
    
    if(!$addonFunc){
        //Nothing to do, functionalities not available
        return;
    }
    
    $entrySet = $App->DirectoryEntrySet();
    
    //The user may be in several directories. We need to check every directories
    $entries = $entrySet->select($entrySet->id_user->is($event->id_user));
    foreach ($entries as $entry){
        /* @var $entry directorymanager_DirectoryEntry */
        /* @var $directory directorymanager_Directory */
        $directory = $entry->directory();
        if($directory){
            $workspace = $directory->getWorkspace();
            if(!$workspace){
                //The directory delegation is not associated to a workspace, do nothing
                continue;
            }
            
            //The directory delegation is associated to a workspace. Get the directory configuration to check if the entry has to be updated
            $config = $addonFunc->getConfiguration($workspace->id);
            if(isset($config['isWorkspaceEnabled']) && isset($config['synchronizeEntries']) && $config['isWorkspaceEnabled'] && $config['synchronizeEntries']){
                //DirectoryManager is enabled for the workspace, and the entry has to be synchronized with the Ovidentia user
                $entry->updateWithOvidentiaUserInfos();
            }
        }
    }
    return;
}

function directorymanager_onAddonUpgraded(bab_eventAddonUpgraded $event)
{
    if ($event->addonName === 'Workspace') {
        directorymanager_instanciateWorkspaceAddon();
    }
}

?>