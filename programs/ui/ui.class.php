<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2018 by CANTICO ({@link http://www.cantico.fr})
 */


/* @var $App Func_App_Directorymanager */
$App = bab_functionality::get('App/Directorymanager');


$App->includeUi();


class directorymanager_Ui extends app_Ui
{
    /**
     * @param Func_App $app
     */
    public function __construct()
    {
        $this->setApp(directorymanager_App());
    }
    
    /**
	 * (non-PHPdoc)
	 * @see programs/app_Object_Interface::setApp()
	 */
    public function setApp(Func_App $app = null)
    {
        $this->app = $app;
        return $this;
    }
    
    
    /**
     * Includes DirectoryEntry Ui helper functions definitions.
     */
    public function includeDirectoryEntry()
    {
        require_once FUNC_DIRECTORYMANAGER_UI_PATH . 'directoryentry.ui.php';
    }
    
    public function includeNotification()
    {
        require_once FUNC_DIRECTORYMANAGER_UI_PATH . 'notification.ui.php';
    }
    
    /**
     * @return directorymanager_DirectoryEntryTableView
     */
    public function DirectoryEntryTableView()
    {
        $this->includeDirectoryEntry();
        return new directorymanager_DirectoryEntryTableView($this->App());
    }
    
    /**
     * @return directorymanager_DirectoryEntryCardsView
     */
    public function DirectoryEntryCardsView()
    {
        $this->includeDirectoryEntry();
        return new directorymanager_DirectoryEntryCardsView($this->App());
    }
    
    public function DirectoryEntryCardFrame(directorymanager_DirectoryEntry $entry)
    {
        $this->includeDirectoryEntry();
        return new directorymanager_DirectoryEntryCardFrame($this->App(), $entry);
    }
    
    public function DirectoryEntryContextMenu(directorymanager_DirectoryEntry $entry, directorymanager_Directory $dir = null)
    {
        $this->includeDirectoryEntry();
        return new directorymanager_DirectoryEntryContextMenu($this->App(), $entry, $dir);
    }
    
    public function DirectoryEntryEditor(directorymanager_DirectoryEntry $entry, $withSubmit = true)
    {
        $this->includeDirectoryEntry();
        return new directorymanager_DirectoryEntryEditor($this->App(), $entry, $withSubmit);
    }
    
    public function DirectoryEntryAddUserToWorkspace(directorymanager_DirectoryEntry $entry)
    {
        $this->includeDirectoryEntry();
        return new directorymanager_DirectoryEntryAddUserToWorkspace($this->App(), $entry);
    }
    
    public function NotifyWorkspaceRegistration($title, $message, $workspace)
    {
        $this->includeNotification();
        return new directorymanager_NotificationTemplate($title, $message, $workspace);
    }
}