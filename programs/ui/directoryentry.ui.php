<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */


bab_functionality::includeOriginal('Icons');
$W = bab_Widgets();


/**
 * list of entries from back office
 *
 */
class directorymanager_DirectoryEntryTableView extends app_TableModelView
{
    protected $directory = null;
    
    /**
     * @param app_CtrlRecord $recordController
     * @return directorymanager_DirectoryEntryTableView
     */
    public function setRecordController(app_CtrlRecord $recordController)
    {
        $this->recordController = $recordController;
        return $this;
    }
    
    
    /**
     * @return app_CtrlRecord
     */
    public function getRecordController()
    {
        return $this->recordController;
    }
    
    /**
     * {@inheritDoc}
     * @see widget_TableModelView::addDefaultColumns()
     */
    public function addDefaultColumns(directorymanager_DirectoryEntrySet $set)
    {
        $App = $this->App();
        $Access = $App->Access();
        
        
        //Here we add every common field. We use bab_translate to use the core translations string
        $this->addColumn(
            widget_TableModelViewColumn(
                $set->cn, 
                bab_translate('Common Name')
            )->setVisible(false)
        );
        $this->addColumn(
            widget_TableModelViewColumn(
                $set->sn, 
                bab_translate('Last Name')
            )
        );
        $this->addColumn(
            widget_TableModelViewColumn(
                $set->mn, 
                bab_translate('Middle Name')
            )->setVisible(false)
        );
        $this->addColumn(
            widget_TableModelViewColumn(
                $set->givenname,
                bab_translate('First Name')
            )
        );
        $this->addColumn(
            widget_TableModelViewColumn(
                $set->email,
                bab_translate('E-mail Address')
            )
        );
        $this->addColumn(
            widget_TableModelViewColumn(
                $set->btel,
                bab_translate('Business Phone')
            )
        );
        $this->addColumn(
            widget_TableModelViewColumn(
                $set->mobile,
                bab_translate('Mobile Phone')
            )
        );
        $this->addColumn(
            widget_TableModelViewColumn(
                $set->htel,
                bab_translate('Home Phone')
            )->setVisible(false)
        );
        $this->addColumn(
            widget_TableModelViewColumn(
                $set->bfax,
                bab_translate('Business Fax')
            )->setVisible(false)
        );
        $this->addColumn(
            widget_TableModelViewColumn(
                $set->title,
                bab_translate('Job Title')
            )->setVisible(false)
        );
        $this->addColumn(
            widget_TableModelViewColumn(
                $set->departmentnumber,
                bab_translate('Department')
            )->setVisible(false)
        );
        $this->addColumn(
            widget_TableModelViewColumn(
                $set->organisationname,
                bab_translate('Company')
            )
        );
        $this->addColumn(
            widget_TableModelViewColumn(
                $set->bstreetaddress,
                bab_translate('Business Street')
            )->setVisible(false)
        );
        $this->addColumn(
            widget_TableModelViewColumn(
                $set->bcity,
                bab_translate('Business City')
            )->setVisible(false)
        );
        $this->addColumn(
            widget_TableModelViewColumn(
                $set->bpostalcode,
                bab_translate('Business Postal Code')
            )->setVisible(false)
        );
        $this->addColumn(
            widget_TableModelViewColumn(
                $set->bstate,
                bab_translate('Business State')
            )->setVisible(false)
        );
        $this->addColumn(
            widget_TableModelViewColumn(
                $set->bcountry,
                bab_translate('Business Country')
            )->setVisible(false)
        );
        $this->addColumn(
            widget_TableModelViewColumn(
                $set->hstreetaddress,
                bab_translate('Home Street')
            )->setVisible(false)
        );
        $this->addColumn(
            widget_TableModelViewColumn(
                $set->hcity,
                bab_translate('Home City')
            )->setVisible(false)
        );
        $this->addColumn(
            widget_TableModelViewColumn(
                $set->hpostalcode,
                bab_translate('Home Postal Code')
            )->setVisible(false)
        );
        $this->addColumn(
            widget_TableModelViewColumn(
                $set->hstate,
                bab_translate('Home State')
            )->setVisible(false)
        );
        $this->addColumn(
            widget_TableModelViewColumn(
                $set->hcountry,
                bab_translate('Home Country')
            )->setVisible(false)
        );
        $this->addColumn(
            widget_TableModelViewColumn(
                $set->user1,
                bab_translate('User 1')
            )->setVisible(false)
        );
        $this->addColumn(
            widget_TableModelViewColumn(
                $set->user2,
                bab_translate('User 2')
            )->setVisible(false)
        );
        $this->addColumn(
            widget_TableModelViewColumn(
                $set->user3,
                bab_translate('User 3')
            )->setVisible(false)
        );
    }
    
    /**
     *
     * @param ORM_Record    $record
     * @param string        $fieldPath
     * @return string       The text of the item that will be placed in the cell
     */
    protected function computeCellTextContent(ORM_Record $record, $fieldPath)
    {
        $extraFields = $record->getExtraFields();
        foreach ($extraFields as $extraField){
            /* @var $extraField directorymanager_DirectoryEntryExtra */
            if($fieldPath == $extraField->babdirid){
                return $extraField->field_value;
            }
        }
        $cellTextContent = self::getRecordFieldValue($record, $fieldPath);
        return $cellTextContent;
    }
    
    protected function computeCellContent(directorymanager_DirectoryEntry $record, $fieldPath){
        if(!isset($this->directory)){
            $filter = $this->getFilterValues();
            if(isset($filter['id_directory'])){
                $App = directorymanager_App();
                $set = $App->DirectorySet();
                /* @var $dir directorymanager_Directory */
                $dir = $set->get($set->id->is($filter['id_directory']));
                if($dir){
                    $this->directory = $dir;
                }
                else{
                    $this->directory = false;
                }
            }
        }
        if($fieldPath === '_actions_'  && $this->directory && $this->directory->isAddableByUser()){
            /* @var $Ui directorymanager_Ui */
            $Ui = $this->App()->Ui();
            return $Ui->DirectoryEntryContextMenu($record, $this->directory);
        }
        return parent::computeCellContent($record, $fieldPath);
    }
}

class directorymanager_DirectoryEntryCardsView extends directorymanager_DirectoryEntryTableView
{
    
    /**
     *
     * @param Func_App_Directorymanager $App
     * @param string $id
     */
    public function __construct(Func_App_Directorymanager $App = null, $id = null)
    {
        parent::__construct($App, $id);
        
        $W = bab_Widgets();
        $layout = $W->FlowLayout()->addClass('directorymanager-card-view');
        $this->setLayout($layout);
    }
    
    /**
     * {@inheritDoc}
     * @see widget_TableModelView::initHeaderRow()
     */
    protected function initHeaderRow(ORM_RecordSet $set)
    {
        return;
    }
    
    /**
     * {@inheritDoc}
     * @see Widget_TableView::addSection()
     */
    public function addSection($id = null, $label = null, $class = null, $colspan = null)
    {
        return $this;
    }
    
    /**
     * {@inheritDoc}
     * @see Widget_TableView::setCurrentSection()
     */
    public function setCurrentSection($id)
    {
        return $this;
    }
    
    
    /**
     * {@inheritDoc}
     * @see widget_TableModelView::handleRow()
     */
    protected function handleRow(ORM_Record $record, $row)
    {
        $App = $this->App();
        $Ui = $App->Ui();
        
        $this->addItem($Ui->DirectoryEntryCardFrame($record));
        
        $row++;
        $this->previousRecord = $record;
        
        return $row;
    }
}


class directorymanager_DirectoryEntryCardFrame extends app_UiObject
{    
    /* @var $entry directorymanager_DirectoryEntry */
    protected $entry = null;
    
    /* @var $entrySet directorymanager_DirectoryEntrySet */
    protected $entrySet = null;
    
    public function __construct(Func_App_Directorymanager $app, directorymanager_DirectoryEntry $entry, $id = null)
    {
        parent::__construct($app);
        
        $W = bab_Widgets();
        $App = $this->App();
        $App->includeDirectoryEntrySet();
        
        $cardLayout = $W->FlowLayout()->addClass('directorymanager-card-layout panel panel-default');
                
        $this->setInheritedItem(
            $W->Frame(
                $id, $cardLayout
            )->addClass('directorymanager-card-frame')
        );
                
        $this->entry = $entry;
        $this->entrySet = $this->entry->getParentSet();
        
        
        $cardLayout->addItem(
            $W->VBoxItems(
                $this->name(),
                $this->organisme(),
                $this->businessAddress(), /** @TODO Add a option to chose between home and business address **/
                $this->businessContactInfos() /** @TODO Add a option to chose between home and business contact info **/
            )->setSizePolicy('widget-100pc')
        );
        
        $this->setSizePolicy('col-xs-12 col-sm-6 col-md-4 col-lg-3 directorymanager-card');
    }
    
    protected function name()
    {
        $App = $this->App();
        $Ui = $App->Ui();
        $W = bab_Widgets();
        
        $box = $W->FlowItems(
            $W->Label(
                $this->entry->getFullName()
            )->addClass('card-title', 'widget-strong')->setSizePolicy('widget-50pc')
        );
        $contextMenu = $Ui->DirectoryEntryContextMenu($this->entry)->setSizePolicy('widget-50pc widget-align-right');

        $menuEmpty = (count($contextMenu->getLayout()->getItems()) == 0);
        if (!$menuEmpty) {
            $box->addItem($contextMenu);
        }
        
        return $box;
    }
    
    protected function organisme()
    {
        $W = bab_Widgets();
        return $W->Label(
            $this->entry->organisationname
        )->addClass('card-subtitle');
    }
    
    protected function homeAddress()
    {
        $W = bab_Widgets();
        $line1 = $this->entry->hstreetaddress;
        $line2 = $this->entry->hpostalcode.' '.$this->entry->hcity.' '.$this->entry->hcountry;
        return $W->VBoxItems(
            $W->Label(
                $line1
            ),
            $W->Label(
                $line2
            )
        );
    }
    
    protected function businessAddress()
    {
        $W = bab_Widgets();
        $line1 = $this->entry->bstreetaddress;
        $line2 = $this->entry->bpostalcode.' '.$this->entry->bcity.' '.$this->entry->bcountry;
        return $W->VBoxItems(
            $W->Label(
                $line1
            ),
            $W->Label(
                $line2
            )
        );
    }
    
    protected function homeContactInfos()
    {
        $W = bab_Widgets();
        
        $infos = $W->VBoxItems()->addClass('widget-align-right');
        
        $fields = array(
            directorymanager_translate('Email')         => 'email',
            directorymanager_translate('Phone')         => 'htel',
            directorymanager_translate('Mobile phone')  => 'mobile',
        );
        
        foreach ($fields as $label => $field)
            if(!empty($this->entry->$field)){
                $infos->addItem(
                    $W->Label(directorymanager_translate($label).' : '.$this->entry->$field)
                );
        }
        
        return $infos;
    }
    
    protected function businessContactInfos()
    {
        $W = bab_Widgets();
        
        $infos = $W->VBoxItems()->addClass('widget-align-right');
        
        $fields = array(
            directorymanager_translate('Email')         => 'email',
            directorymanager_translate('Phone')         => 'btel',
            directorymanager_translate('Fax')           => 'bfax',
            directorymanager_translate('Mobile phone')  => 'mobile',
        );
        
        foreach ($fields as $label => $field)
        if(!empty($this->entry->$field)){
            $infos->addItem(
                $W->Label(directorymanager_translate($label).' : '.$this->entry->$field)    
            );
        }

        return $infos;
    }
}

class directorymanager_DirectoryEntryContextMenu extends app_UiObject
{

    /* @var $entry directorymanager_DirectoryEntry */
    protected $entry = null;
    
    /* @var $entrySet directorymanager_DirectoryEntrySet */
    protected $entrySet = null;
    
    /* @var $directory directorymanager_Directory */
    protected $directory = null;
    
    public function __construct(Func_App_Directorymanager $app, directorymanager_DirectoryEntry $entry, directorymanager_Directory $dir = null, $id = null)
    {
        parent::__construct($app);
        
        $W = bab_Widgets();
        $App = $this->App();
        
        $contextMenuLayout = $W->FlowLayout();
        
        $this->setInheritedItem($W->Frame($id, $contextMenuLayout));
        
        $this->entry = $entry;
        $this->entrySet = $this->entry->getParentSet();
        $this->directory = isset($dir) ? $dir : $entry->directory();
        
        $contextMenuLayout->addItem(
            $this->contextMenu()
        );
    }
    
    public function contextMenu()
    {
        $W = bab_Widgets();
        $App = $this->App();
        
        /* @var $ctrl directorymanager_CtrlDirectoryEntry */
        $ctrl = $App->Controller()->DirectoryEntry();
        
        $contextMenu = $W->Menu();
        $contextMenu->setLayout($W->FlowLayout());
        $contextMenu->addClass(Func_Icons::ICON_LEFT_16);
        $contextMenu->setSizePolicy('widget-actions');
        
        if($this->directory && $this->directory->isEditableByUser()){
            $contextMenu->addItem(
                $W->Link(
                    directorymanager_translate('Update'),
                    $ctrl->edit($this->entry->id)
                )->addClass('icon', Func_Icons::ACTIONS_DOCUMENT_EDIT)
                ->setOpenMode(Widget_Link::OPEN_DIALOG_AND_RELOAD)
            );
            $contextMenu->addItem(
                $W->Link(
                    directorymanager_translate('Delete'),
                    $ctrl->confirmDelete($this->entry->id)
                )->addClass('icon', Func_Icons::ACTIONS_EDIT_DELETE)
            ->setOpenMode(Widget_Link::OPEN_DIALOG_AND_RELOAD)
            );
        }
        
        return $contextMenu;
    }
}

$W->includePhpClass('Widget_Form');


class directorymanager_DirectoryEntryEditor extends Widget_Form
{
    /**
     * @var directorymanager_DirectoryEntry
     */
    protected $record = null;
    
    /**
     * @var Func_App_Directorymanager
     */
    private $app = null;
    
    /**
     * @param	Func_App_Directorymanager $app
     * @param 	Widget_Layout 	          $layout  The layout that will manage how widgets are displayed in this form.
     * @param 	string 			          $id      The item unique id.
     */
    public function __construct(Func_App_Directorymanager $app, directorymanager_DirectoryEntry $entry, $withSubmit = true, $id = null, Widget_Layout $layout = null)
    {
        $W = bab_Widgets();
        
        // We simulate inheritance from Widget_VBoxLayout.
        
        $this->setApp($app);
        $this->setRecord($entry);
        $this->setName('data');
        
        $this->innerLayout = $layout;
        $this->buttonsLayout = $this->buttonsLayout();
        $this->buttonsLayout->setSizePolicy('widget-list-element');
        
        if (!isset($this->innerLayout)) {
            $this->innerLayout = $W->FlowLayout()->setSizePolicy('row');
        }
        
        $this->innerLayout->addClass('crm-loading-box'); // javascript can add the crm-loading-on class to this container to show ajax loading progress in the form
        
        $layout = $W->VBoxItems(
            $this->innerLayout,
            $this->buttonsLayout
        );
        $layout->setVerticalSpacing(2, 'em');
        
        parent::__construct($id, $layout);
        $this->addClass(Func_Icons::ICON_LEFT_16);
        $this->setHiddenValue('tg', $app->controllerTg);
        $this->setHiddenValue('data[id]', $entry->id);
        $this->setHiddenValue('data[id_directory]', $entry->id_directory);
        $this->prependFields();
        if($withSubmit){
            $this->appendButtons();
        }
    }
    
    /**
     * Fields that will appear at the beginning of the form.
     *
     */
    protected function prependFields()
    {
        $W = bab_Widgets();
        
        $directoryFields = $this->record->getDirectoryFields();
        $keyFields = array();
        foreach ($directoryFields as $field){
            $keyFields[$field['name']] = $field['description'];
        }
                     
        //COORDINATES
        
        $hasCoordinates = false;
        $coordinateFields = array(
            'givenname',
            'sn',
            'organisationname',
            'departmentnumber',
            'btel',
            'mobile',
            'bfax',
            'email'
        );   
        
        $coordinatesSection = $W->Section(
            directorymanager_translate('Coordinates'),
            $coordinatesBox = $W->FlowLayout()->setSizePolicy('row')
        )->setFoldable(true, false);
        
        foreach ($coordinateFields as $field){
            if(isset($keyFields[$field])){
                $hasCoordinates = true;
                switch($field){
                    case 'givenname':
                    case 'sn':
                        $coordinatesBox->addItem(
                            $W->FlowItems(
                                $W->LabelledWidget(
                                    $keyFields[$field],
                                    $W->LineEdit()->setName($field)->setValue($this->record->getValue($field))->setMandatory(true)
                                )->setSizePolicy('widget-100pc')
                            )->setSizePolicy('col-md-6 col-xs-12 form-group')->addClass('widget-100pc')
                        );
                        break;
                    case 'organisationname':
                    case 'departmentnumber':
                        $coordinatesBox->addItem(
                            $W->FlowItems(
                                $W->LabelledWidget(
                                    $keyFields[$field],
                                    $W->LineEdit()->setName($field)->setValue($this->record->getValue($field))
                                )->setSizePolicy('widget-100pc')
                            )->setSizePolicy('col-xs-12 form-group')->addClass('widget-100pc')
                        );
                        break;
                    case 'btel':
                    case 'mobile':
                    case 'bfax':
                        $coordinatesBox->addItem(
                            $W->FlowItems(
                                $W->LabelledWidget(
                                    $keyFields[$field],
                                    $W->LineEdit()->setName($field)->setValue($this->record->getValue($field))
                                )->setSizePolicy('widget-100pc')
                            )->setSizePolicy('col-md-4 col-xs-12 form-group')->addClass('widget-100pc')
                        );
                        break;
                    case 'email':
                        $coordinatesBox->addItem(
                            $W->FlowItems(
                                $W->LabelledWidget(
                                    $keyFields[$field],
                                    $W->EmailLineEdit()
                                        ->setName($field)
                                        ->setValue($this->record->getValue($field))
                                        ->setMandatory(true)
                                        ->setSizePolicy('widget-100pc')
                                        ->addClass('widget-100pc')
                                )->setSizePolicy('widget-100pc')
                            )->setSizePolicy('col-xs-12 form-group')->addClass('widget-100pc')
                        );
                        break;
                    default:
                        break;
                }
            }
        }
        
        //ADDRESS
        
        $hasAddress = false;
        $businessAddressFields = array(
            'bstreetaddress',
            'bpostalcode',
            'bcity',
            'bstate',
            'bcountry'
        );
        
        $addressSection = $W->Section(
            directorymanager_translate('Address'),
            $addressBox = $W->FlowLayout()->setSizePolicy('row')
        )->setFoldable(true, true);
            
        foreach ($businessAddressFields as $field){
            if(isset($keyFields[$field])){
                $hasAddress = true;
                switch($field){
                    case 'bstreetaddress':
                        $addressBox->addItem(
                            $W->FlowItems(
                                $W->LabelledWidget(
                                    $keyFields[$field],
                                    $W->LineEdit()->setName($field)->setValue($this->record->getValue($field))
                                )->setSizePolicy('widget-100pc')
                            )->setSizePolicy('col-xs-12 form-group')->addClass('widget-100pc')
                        );
                        break;
                    case 'bpostalcode':
                        $addressBox->addItem(
                            $W->FlowItems(
                                $W->LabelledWidget(
                                    $keyFields[$field],
                                    $W->LineEdit()->setName($field)->setValue($this->record->getValue($field))
                                )->setSizePolicy('widget-100pc')
                            )->setSizePolicy('col-md-4 col-xs-12 form-group')->addClass('widget-100pc')
                        );
                        break;
                    case 'bcity':
                        $addressBox->addItem(
                            $W->FlowItems(
                                $W->LabelledWidget(
                                    $keyFields[$field],
                                    $W->LineEdit()->setName($field)->setValue($this->record->getValue($field))
                                )->setSizePolicy('widget-100pc')
                            )->setSizePolicy('col-md-8 col-xs-12 form-group')->addClass('widget-100pc')
                        );
                        break;
                    case 'bstate':
                    case 'bcountry':
                        $addressBox->addItem(
                            $W->FlowItems(
                                $W->LabelledWidget(
                                    $keyFields[$field],
                                    $W->LineEdit()->setName($field)->setValue($this->record->getValue($field))
                                )->setSizePolicy('widget-100pc')
                            )->setSizePolicy('col-md-6 col-xs-12 form-group')->addClass('widget-100pc')
                        );
                        break;
                    default:
                        break;
                }
            }
        }
        
        //OTHER
        
        $hasOther = false;
        $otherSection = $W->Section(
            directorymanager_translate('Miscellaneous'),
            $otherBox = $W->FlowLayout()->setSizePolicy('row')
        )->setFoldable(true, true);
        
        
        //HOME ADDRESS
        
        $hasHomeAddress = false;
        $homeAddressFields = array(
            'hstreetaddress',
            'hpostalcode',
            'hcity',
            'hstate',
            'hcountry'
        );
        
        $homeAddressSection = $W->Section(
            directorymanager_translate('Address (home)'),
            $homeAddressBox = $W->FlowLayout()->setSizePolicy('row')
        )->setFoldable(true, true)->setSizePolicy('col-xs-12');
        
        foreach ($homeAddressFields as $field){
            if(isset($keyFields[$field])){
                $hasOther = true;
                $hasHomeAddress = true;
                switch($field){
                    case 'hstreetaddress':
                        $homeAddressBox->addItem(
                            $W->FlowItems(
                                $W->LabelledWidget(
                                    $keyFields[$field],
                                    $W->LineEdit()->setName($field)->setValue($this->record->getValue($field))
                                )->setSizePolicy('widget-100pc')
                            )->setSizePolicy('col-xs-12 form-group')->addClass('widget-100pc')
                        );
                        break;
                    case 'hpostalcode':
                        $homeAddressBox->addItem(
                            $W->FlowItems(
                                $W->LabelledWidget(
                                    $keyFields[$field],
                                    $W->LineEdit()->setName($field)->setValue($this->record->getValue($field))
                                )->setSizePolicy('widget-100pc')
                            )->setSizePolicy('col-md-4 col-xs-12 form-group')->addClass('widget-100pc')
                        );
                        break;
                    case 'hcity':
                        $homeAddressBox->addItem(
                            $W->FlowItems(
                                $W->LabelledWidget(
                                    $keyFields[$field],
                                    $W->LineEdit()->setName($field)->setValue($this->record->getValue($field))
                                )->setSizePolicy('widget-100pc')
                            )->setSizePolicy('col-md-8 col-xs-12 form-group')->addClass('widget-100pc')
                        );
                        break;
                    case 'hstate':
                    case 'hcountry':
                        $homeAddressBox->addItem(
                            $W->FlowItems(
                                $W->LabelledWidget(
                                    $keyFields[$field],
                                    $W->LineEdit()->setName($field)->setValue($this->record->getValue($field))
                                )->setSizePolicy('widget-100pc')
                            )->setSizePolicy('col-md-6 col-xs-12 form-group')->addClass('widget-100pc')
                        );
                        break;
                    default:
                        break;
                }
            }
        }
        
        //EXTRA FIELDS
        
        $hasExtra = false;
        
        $extraSection = $W->Section(
            directorymanager_translate('Extra fields'),
            $extraBox = $W->FlowLayout()->setSizePolicy('row')
        )->setFoldable(true, true)->setSizePolicy('col-xs-12');
        
        $extraFields = $this->record->getExtraFields();
        foreach ($extraFields as $extraField){
            /* @var $extraField directorymanager_DirectoryEntryExtra */
            $hasCustom = true;
            $hasExtra = true;
            $extraBox->addItem(
                $W->FlowItems(
                    $W->LabelledWidget(
                        $extraField->getName(),
                        $W->LineEdit()->setName($extraField->babdirid)->setValue($extraField->field_value)
                    )->setSizePolicy('widget-100pc')
                )->setSizePolicy('col-xs-12 form-group')->addClass('widget-100pc')
            );
        }
        
        //CUSTOM
        
        $hasCustom = false;
        $customFields = array(
            'user1',
            'user2',
            'user3'
        );
        
        $customSection = $W->Section(
            directorymanager_translate('Other'),
            $customBox = $W->FlowLayout()->setSizePolicy('row')
        )->setFoldable(true, true)->setSizePolicy('col-xs-12');
        
        foreach ($customFields as $field){
            if(isset($keyFields[$field])){
                $hasCustom = true;
                $customBox->addItem(
                    $W->FlowItems(
                        $W->LabelledWidget(
                            $keyFields[$field],
                            $W->LineEdit()->setName($field)->setValue($this->record->getValue($field))
                        )->setSizePolicy('widget-100pc')
                    )->setSizePolicy('col-xs-12 form-group')->addClass('widget-100pc')
                );
            }
        }
        
        if($hasHomeAddress){
            $otherBox->addItem($homeAddressSection);
        }
        if($hasExtra){
            $otherBox->addItem($extraSection);
        }
        if($hasCustom){
            $otherBox->addItem($customSection);
        }
        
        if($hasCoordinates){
            $this->innerLayout->addItem($coordinatesSection->setSizePolicy('col-xs-12'));
        }
        if($hasAddress){
            $this->innerLayout->addItem($addressSection->setSizePolicy('col-xs-12'));
        }
        if($hasOther){
            $this->innerLayout->addItem($otherSection->setSizePolicy('col-xs-12'));
        }
    }
    
    protected function appendButtons()
    {
        $W = bab_Widgets();
        $App = $this->app;

        $saveLabel = isset($this->saveLabel) ? $this->saveLabel : $App->translate('Save');
        $submitButton = $W->SubmitButton();
        $submitButton->addClass('btn btn-primary');
        $submitButton->validate(true)
            ->setAction($App->Controller()->DirectoryEntry()->save())
            ->setLabel($saveLabel);
        $submitButton->setAjaxAction();
        $this->addButton($submitButton);
        
        $cancelLabel = isset($this->cancelLabel) ? $this->cancelLabel : $App->translate('Cancel');
        $this->addButton(
            $W->SubmitButton()
            ->addClass('widget-close-dialog')
            ->setLabel($cancelLabel)
        );
    }
    
    public function addButton(Widget_Item $button, Widget_Action $action = null)
    {
        $this->buttonsLayout->addItem($button);
        if (isset($action)) {
            $button->setAction($action);
        }
        
        return $this;
    }
    
    public function buttonsLayout()
    {
        $W = bab_Widgets();
        return $W->HBoxLayout()->setHorizontalSpacing(1, 'em');
    }
    
    /**
     * @param Func_App_Directorymanager $app
     * @return self
     */
    public function setApp(Func_App_Directorymanager $app = null)
    {
        $this->app = $app;
        return $this;
    }
    
    /**
     *
     * @return self
     */
    public function setSaveAction($saveAction, $saveLabel = null)
    {
        $this->saveAction = $saveAction;
        $this->saveLabel = $saveLabel;
        return $this;
    }
    
    /**
     * Set record and values to editor
     *
     * @param directorymanager_DirectoryEntry $record
     *
     * @return self
     */
    public function setRecord(directorymanager_DirectoryEntry $record)
    {
        $this->record = $record;
        if ($name = $this->getName()) {
            $values = $record->getFormOutputValues();
            $this->setValues($values, array($name));
        }
        
        return $this;
    }
}

class directorymanager_DirectoryEntryAddUserToWorkspace extends directorymanager_DirectoryEntryEditor
{
    protected function prependFields()
    {
        $W = bab_Widgets();
        
        $directory = $this->record->directory();
        $workspaceConfiguration = $directory->getWorkspaceConfiguration();
        
        
        
        if(isset($workspaceConfiguration['allowAccountCreation']) && $workspaceConfiguration['allowAccountCreation'] == 1){
            $tabs = $W->Tabs();
            
            $tabs->addTab(
                directorymanager_translate('Select an existing user'),
                $this->selectUser()
            );
            
            $tabs->addTab(
                directorymanager_translate('Create a new user'),
                $this->newUser()
            );
            
            $this->innerLayout->addItem($tabs);
        }
        else{
            $this->innerLayout->addItem($this->selectUser());
        }
    }
    
    protected function newUser()
    {
        $App = directorymanager_App();
        return $App->Controller()->DirectoryEntry(false)->createNewUser($this->record->id_directory);
    }
    
    protected function selectUser()
    {
        $App = directorymanager_App();
        $W = bab_Widgets();
        $selectUserBox = $W->VBoxItems();
        
        $userPicker = $W->UserPicker();
        $userPicker->setName('existingUser');
        $userPicker->setSizePolicy(Func_Icons::ICON_LEFT_16);
        $userPicker->addClass('form-control')->addClass('widget-fullwidth')->setSizePolicy('form-group');

        $selectUserBox->addItem(
            $W->LabelledWidget(
                directorymanager_translate('New member'),
                $userPicker
            )
        );
        
        $selectUserBox->addItem(
            $W->LabelledWidget(
                directorymanager_translate('Notify user'),
                $W->CheckBox()->setName('notifyUser')
            )    
        );
        
        $selectUserBox->addItem(
            $W->SubmitButton()
                ->setLabel(directorymanager_translate('Add user'))
                ->setAction($App->Controller()->DirectoryEntry()->saveUserToWorkspace())
                ->setAjaxAction()
        );
        
        return $selectUserBox;
    }
    
    public function addButton()
    {
        return $this;
    }
}