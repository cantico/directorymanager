<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2018 by CANTICO ({@link http://www.cantico.fr})
 */

require_once $GLOBALS['babInstallPath'] . 'utilit/template.php';

class directorymanager_NotificationTemplate extends bab_Template
{
    
    public $title;
    public $message;
    
    public $workspaceLabel;
    public $workspaceName;
    
    public $dateLabel;
    public $date;
    
    public $workspaceUrl;
    
    public function __construct($title, $message, workspace_Workspace $workspace)
    {
        $App = workspace_App();
        
        $this->message = $message;
        $this->title = $title;
        
        $this->workspaceLabel = directorymanager_translate('Workspace');
        $this->workspaceName = $workspace->name;
        $this->workspaceUrl = $GLOBALS['babUrl'] . $App->Controller()->Page()->display('home', $workspace->id)->url();
        
        $this->dateLabel = directorymanager_translate('Date');
        $this->date = bab_strftime(mktime());
    }
}