��    =        S   �      8     9     G     P     j     r  -   �  ,   �  0   �  )     '   7     _     {     �     �     �     �     �     �     �     �     �     �     �       
        '     -     :     G  !   ]       
   �     �     �     �     �     �     �  	   �     �     �  .        >     Z     u  (   �  -   �  /   �     	  ;   	  
   X	     c	  &   v	  	   �	  $   �	  %   �	  (   �	  '   
     C
     _
  �  z
     y     �  /   �     �     �  7   �  8   "  B   [  :   �  9   �          -     5     <     I     f     w  	   |     �  	   �     �     �     �     �     �               0     E  >   ^     �     �     �     �     �     �     �            %     (   C  7   l     �      �  "   �  6     8   :  9   s     �  R   �     	           3     T  %   h  4   �  |  �  2  @  �   s  �   1     =   ,                   6               '       .                     ;          8   /       %       
   "       3          <         9              #         4            0   :   )       +         (          *   2          7         $   1      5   &                  !      	          -              Access denied Add user Add user to the workspace Address Address (home) Allow Worskpace addon to use DirectoryManager An account with this nickname already exists An error occured while creating the user account An error occured while deleting the entry An error occured while saving the entry Authentication informations Cancel Cards Coordinates Create a new user Create entry Date Delete Details Directories Directory Management Directory Manager Directory to display Display type Edit entry Email Extra fields Invalid data Invalid email address Managers can create user accounts Miscellaneous New member Notify the user Notify user Other Password Portal tools Save Search... Select an existing user Send login and password The directory is not associated to a workspace The directory was not found The entry has been deleted The entry has been saved The user has been added to the workspace There is no group associated to the workspace This directory is not associated to a workspace Update Update the directory entry when the portal entry is updated User login User was not found We have a problem finding this page... Workspace You do not have access to this page. You have been invited to workspace %s __NEW_MEMBER_MESSAGE_ACCOUNTINFOS_HTML__ __NEW_MEMBER_MESSAGE_ACCOUNTINFOS_TXT__ __NEW_MEMBER_MESSAGE_HTML__ __NEW_MEMBER_MESSAGE_TXT__ Project-Id-Version: 
POT-Creation-Date: 2018-11-30 16:29+0100
PO-Revision-Date: 2018-11-30 16:30+0100
Last-Translator: Robin Bailleux <robin.bailleux@cantico.fr>
Language-Team: 
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.0.7
X-Poedit-Basepath: ../programs
Plural-Forms: nplurals=2; plural=(n > 1);
X-Poedit-KeywordsList: directorymanager_translate;directorymanager_translate:1,2;translate:1,2;translate
X-Poedit-SearchPath-0: .
 Accès refusé Ajouter un utilisateur Ajouter un utilisateur à l'espace collaboratif Adresse Adresse (domicile) Permet à l'addon Workspace d'utiliser DirectoryManager Un compte utilisateur avec cet identifiant existe déjà Une erreur est survenue lors de la création du compte utilisateur Une erreur est survenue lors de la suppression de la fiche Une erreur est survenue lors de la sauvegarde de la fiche Informations de connexion Annuler Cartes Coordonnées Créer un nouvel utilisateur Créer une fiche Date Supprimer Détails Annuaires Gestionnaire d'annuaires Gestionnaire d'annuaires Annuaire à afficher Type d'affichage Éditer la fiche Adresse de messagerie Champs supplémentaires Données incorrectes Adresse email incorrecte Autoriser les gestionnaires à créer des comptes utilisateurs Divers Nouveau membre Notifier l'utilisateur Notifier l'utilisateur Autre Mot de passe Outils portail Sauvegarder Rechercher... Sélectionner un utilisateur existant Envoyer l'identifiant et le mot de passe L'annuaire n'est pas associé à un espace collaboratif L'annuaire est introuvable La fiche a bien été supprimée La fiche a bien été enregistrée L'utilisateur a été ajouté à l'espace collaboratif Il n'y a pas de groupe associé à l'espace collaboratif Cet annuaire n'est pas associé à un espace collaboratif Modifier Mettre à jour la fiche de l'annuaire lorsque la fiche du portail est mise à jour Identifiant L'utilisateur est introuvable Impossible de trouver la page… Espace collaboratif Vous n'avez pas accès à cette page. Vous avez été invité sur l'espace collaboratif %s Bonjour,<br /><br />Vous avez été invité sur l'espace collaboratif <b>%s</b>.<br /><br />Vous pouvez accéder au site cliquant sur le lien suivant :<br />%s<br /><br />Vos informations d'authentification sont les suivantes :<br />Identifiant : %s<br />Mot de passe : %s<br /> Pensez à changer votre mot de passe lors de votre première connexion.<br /><br />Cordialement<br /> Bonjour,

Vous avez été invité sur l'espace collaboratif %s.

Vous pouvez accéder au site cliquant sur le lien suivant :
%s

Vos informations d'authentification sont les suivantes :
Identifiant : %s
Mot de passe : %s
Pensez à changer votre mot de passe lors de votre première connexion.

Cordialement Bonjour,<br /><br />Vous avez été invité sur l'espace collaboratif <b>%s</b>.<br /><br />Vous pouvez accéder au site cliquant sur le lien suivant :<br />%s<br /><br />Cordialement<br /> Bonjour,

Vous avez été invité sur l'espace collaboratif %s.

Vous pouvez accéder au site cliquant sur le lien suivant :
%s

Cordialement 